/**
 *  @headerfile ezrc/memory/arena.h
 *  @brief      Contains the API of the ezrc arena allocator
 */

#ifndef EZRC_ARENA
#define EZRC_ARENA

#include <stdlib.h>
#include <stdint.h>

#include <ezrc/base_structs.h>
#include <ezrc/error_codes.h>
#include <ezrc/containers/linked_list.h>

/**
 *  @struct ezrc_arena_cfg
 *  @brief  Struct containing the configuration of an ezrc_arena. See the EZRC_DEFAULT_ARENA_CFG macro for the default configuration.
 */
struct ezrc_arena_cfg
{
    /** determines how granular the allocations can be. ``1 << size_gran`` will be the smallest block size. */
    size_t size_gran;
    /** determines when the allocator needs to use a dedicated page. If the allocation is larger
     *  than ``getpagesize () - large_mem_rest_thres`` a dedicated page will be used
     */
    size_t large_mem_rest_thres;
    union {
        struct {
            /** if static_size is set to 1, the allocator will allocate memory at init and will fail if that memory is used up.
             *  Otherwise it will allocate more regions if the initial region is used up.
             */
            unsigned int static_size    : 1;
        };
        unsigned int all_flags;
    };
};

/**
 * @struct ezrc_arena_region
 * @brief  Struct containing the header of an arena region.
 *         An arena region is typically a page of memory with this header at the start.
 */
struct ezrc_arena_region
{
    struct ezrc_linked_list_node ll_region;
    size_t blk_used_size;
    size_t capacity;
    size_t largest_free_blk;
    /** first pointer to the memory region not yet part of a block */
    void *ptr_last;
    struct ezrc_linked_list_head free_blks;
    struct ezrc_linked_list_head used_blks;
    struct ezrc_arena_block *last_blk;
    uintptr_t mem_data[];
};

/**
 * @struct ezrc_arena_block
 * @brief  Structure of a memory block of the arena.
 *         A memory block is a part of a memory region that has been used.
 */
struct ezrc_arena_block
{
    struct ezrc_linked_list_node ll;
    struct ezrc_arena_block *prev;
    struct ezrc_arena_region *reg;
    size_t size;
    unsigned int free      : 1;
    unsigned int full_page : 1;
    uintptr_t mem_data[];
};

/**
 * @struct ezrc_arena_page
 * @brief  Structure of a memory region that contains a single block, i.e. a page dedicated to
 *         a single allocation because that allocation went over the ``large_mem_rest_thres``.
 */
struct ezrc_arena_page
{
    struct ezrc_linked_list_node ll_page;
    struct ezrc_arena_block blk;
};

/**
 * @struct ezrc_arena
 * @brief  Structure of an arena context. This needs to be initialized with an ``ezrc_arena_cfg``.
 */
struct ezrc_arena
{
#ifdef EZRC_HAS_PTHREAD
    int threadsafe;
#endif
    struct ezrc_arena_cfg cfg;
    size_t size_mask;
    struct ezrc_linked_list_head regions;
    struct ezrc_linked_list_head pages;
};

/**
 * @brief Initialize an arena context.
 * @param arena pointer to the arena context to initialize.
 * @param cfg   configuration to use for the arena. If you're not sure, you can use EZRC_DEFAULT_ARENA_CFG.
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_arena (struct ezrc_arena *arena, struct ezrc_arena_cfg cfg);

/**
 * @brief allocate using an arena allocator
 * @param size size to allocate
 * @param data pointer to an arena context
 */
void *ezrc_arena_alloc (const size_t size, void *data);

/**
 * @brief reallocate data so that ``ptr`` points to memory that can hold ``new_size`` bytes
 * @param ptr pointer to memory region to reallocate. Needs to be allocated with the same arena as the one ``data`` points to.
 * @param new_size new size of memory region
 * @param data pointer to an arena context.
 */
void *ezrc_arena_realloc (void *ptr, const size_t new_size, void *data);

/**
 * @brief free memory block of pointer
 * @param ptr pointer to the memory region of the block to free. Needs to be allocated with the same arena as the one ``data`` points to.
 * @param data pointer to an arena context.
 */
void ezrc_arena_free (void *ptr, void *data);

/**
 * @brief clear all data of the arena. All data will be freed and the memory arena can be used again.
 *        It will keep 1 memory region mapped to start allocating. So the arena is in the same state
 *        as if it has just been initialized.
 * @param arena arena to clear
 */
void ezrc_arena_clear (struct ezrc_arena *arena);

/**
 * @brief Free the arena completely. If you want to use it again, you'll need to reinitialize it again.
 * @param arena arena to clean up
 */
void ezrc_arena_cleanup (struct ezrc_arena *arena);

/**
 * @brief Default arena configuration.
 */
#define EZRC_DEFAULT_ARENA_CFG (struct ezrc_arena_cfg) { \
    .size_gran = 8,                                      \
    .large_mem_rest_thres = 512,                         \
    .all_flags = 0,                                      \
}

/**
 * @brief macro to create a `struct ezrc_allocator` from an arena context
 * @param arena *pointer* to an arena contaext
 */
#define EZRC_ARENA_ALLOCATOR(arena) (struct ezrc_allocator) { \
    .alloc = ezrc_arena_alloc,                                \
    .realloc = ezrc_arena_realloc,                            \
    .free = ezrc_arena_free,                                  \
    .data = arena,                                            \
}

#ifdef EZRC_HAS_PTHREAD

#include <pthread.h>

/**
 * @struct ezrc_arena_ts
 * @brief  Structure of a threadsafe arena context. This needs to be initialized with an ``ezrc_arena_cfg``.
 */
struct ezrc_arena_ts
{
#ifdef EZRC_HAS_PTHREAD
    int threadsafe;
#endif
    struct ezrc_arena_cfg cfg;
    size_t size_mask;
    struct ezrc_linked_list_head regions;
    struct ezrc_linked_list_head pages;
    pthread_rwlock_t rwlock;
};

/**
 * @brief Initialize a threadsafe arena context.
 * @param arena pointer to the arena context to initialize.
 * @param cfg   configuration to use for the arena. If you're not sure, you can use EZRC_DEFAULT_ARENA_CFG.
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_arena_ts (struct ezrc_arena_ts *arena, struct ezrc_arena_cfg cfg);

/**
 * @brief Manually aquire a read lock. multiple threads can hold a read lock at the same time.
 * @param arena threadsafe arena
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_arena_get_rd_lock (struct ezrc_arena_ts *arena);

/**
 * @brief Manually aquire a write lock. Only one write lock can exist. No read locks exist when a write lock exists.
 * @param arena threadsafe arena
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_arena_get_wr_lock (struct ezrc_arena_ts *arena);

/**
 * @brief Manually release a read or write lock
 * @param arena threadsafe arena
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_arena_release_lock (struct ezrc_arena_ts *arena);

#endif

#endif
