/**
 * @headerfile ezrc/memory/allocator.h
 * @brief      Contains the base api for the ezrc_allocator
 */

#ifndef EZRC_ALLOCATOR_H
#define EZRC_ALLOCATOR_H

#include <stdlib.h>

#include "ezrc/base_structs.h"

/**
 * @brief allocate something with the passed ezrc_allocator
 * @param allocator ezrc_allocator to use
 * @param size      size to be allocated
 * @return pointer to the allocated memory or NULL on failure
 */
void *ezrc_allocator_alloc (struct ezrc_allocator allocator, const size_t size);

/**
 * @brief reallocate something with the passed ezrc_allocator
 * @param allocator ezrc_allocator to use
 * @param ptr       pointer to memory region that needs resizing
 * @param new_size  new size for the pointer
 * @return pointer to the new memory block or NULL on failure. Behavior on failure depends on allocator.
 */
void *ezrc_allocator_realloc (struct ezrc_allocator allocator, void *ptr, const size_t new_size);

/**
 * @brief free pointer with the given allocator. Always use the same allocator as used for the allocation itself
 * @param allocator ezrc_allocator to use
 * @param ptr       pointer to free.
 */
void  ezrc_allocator_free (struct ezrc_allocator allocator, void *ptr);

/**
 *      STDLIB WRAPPERS
 *      ===============
 */

/**
 * @brief wrapper around the stdlib malloc function so that it follows the convention
 *        that is expected by the ezrc_allocator struct.
 * @param size Size to be allocated from the heap
 * @param data Context data. Will be ignored in this wrapper
 * @return pointer to the allocated memory or NULL on failure
 */
void *ezrc_std_heap_malloc_wrapper (const size_t size, void *data);

/**
 * @brief wrapper around the stdlib realloc function so that it follows the convention
 *        that is expected by the ezrc_allocator struct.
 * @param ptr      pointer to resize
 * @param new_size New size to be reallocated on the heap
 * @param data     Context data. Will be ignored in this wrapper
 * @return pointer to the new memory block or NULL on failure.
 *         When successful, the old pointer is not valid anymore.
 *         When unsuccessful, the old pointer is still a valid memory address.
 */
void *ezrc_std_heap_realloc_wrapper (void *ptr, const size_t new_size, void *data);

/**
 * @brief wrapper around the stdlib free function so that it follows the convention
 *        that is expected by the ezrc_allocator struct.
 * @param ptr  pointer to free.
 * @param data Context data. Will be ignored in this wrapper
 */
void  ezrc_std_heap_free_wrapper (void *ptr, void *data);

/**
 * @brief This macro can be used to pass an ezrc_allocator struct representing the
 *        heap allocator from the included standard C library
 */
#define EZRC_DEFAULT_ALLOCATOR                         \
    (struct ezrc_allocator)                            \
    {                                                  \
        .alloc        = ezrc_std_heap_malloc_wrapper,  \
        .realloc      = ezrc_std_heap_realloc_wrapper, \
        .free         = ezrc_std_heap_free_wrapper,    \
        .data         = NULL,                          \
    }

#endif
