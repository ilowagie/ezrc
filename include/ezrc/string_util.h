/**
 * @headerfile ezrc/string_util.h
 * @brief      Contains some helpers for working with strings
 */

#ifndef EZRC_STING_UTIL_H
#define EZRC_STING_UTIL_H

#include "ezrc/base_structs.h"
#include "ezrc/error_codes.h"

/**
 * @brief will allocate a `char *` with the exact size needed for the given format string
 * @param allocator allocator to use
 * @param buf       pointer to the string pointer. is expected to be unallocated!
 * @param fmt       format string
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_create_format_string (struct ezrc_allocator allocator, char **buf, char *fmt, ...);

/**
 * @brief strdup implementation where you can define the allocator used for the new buffer.
 *        will use the static_alloc function in the allocator to allocate the new buffer.
 * @param allocator allocator to use
 * @param string    string to be duplicated
 * @param info      will contain info about potential failures if not NULL.
 * @returns a newly allocated string with the same contents as string, or NULL on failure
 */
char *ezrc_strdup_static (struct ezrc_allocator allocator, const char *string, struct ezrc_return *info);

#endif
