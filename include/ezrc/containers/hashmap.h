/**
 * @headerfile ezrc/containers/hashmap.h
 * @brief      Contains the API for the ezrc hashmap container.
 */

#ifndef EZRC_HASHMAP
#define EZRC_HASHMAP

#include <stdlib.h>
#include <stdint.h>
#include <xxhash.h>

#include <ezrc/base_structs.h>
#include <ezrc/error_codes.h>
#include <ezrc/containers/linked_list.h>

/**
 * @struct ezrc_hashmap_cfg
 * @brief  struct containing all the configurations of the hashmap
 */
struct ezrc_hashmap_cfg
{
    size_t initial_nrof_rows;
    /** if ``sizeof_key`` is 0, the size will be obtained with ``get_key_len``, otherwise, ``get_key_len`` can be ``NULL`` */
    size_t sizeof_key;
    size_t (*get_key_len) (const void *);
    /** if ``sizeof_val`` is 0, the size will be obtained with ``get_val_len``, otherwise, ``get_val_len`` can be ``NULL`` */
    size_t sizeof_val;
    size_t (*get_val_len) (const void *);
    /** The ``max_load_factor`` and ``min_load_factor`` will determine when the array of rows needs to be reallocated . */
    double max_load_factor;
    double min_load_factor;
    /** after this amount of size altering operations. the hashmap will check if it needs to resize */
    unsigned int size_check_intv : 8;
    /** set to 1 if the key also needs to be copied, otherwise it will just store a pointer */
    unsigned int store_key : 1;
    /** set to 1 if the val also needs to be copied, otherwise it will just store a pointer */
    unsigned int store_val : 1;
    /**
     * If set to 1, the amount of rows can grow to more than the initial number.
     * If 0 the min and max load factor will be ignored.
     */
    unsigned int allow_resize : 1;
    /**
     * If set to 1, when inserting the same key twice, the value will be overwritten.
     * If 0, trying to insert the same key will result in an error
     */
    unsigned int allow_overwrite : 1;
    /** If set to 1, a full memory compare of the keys will be done if the same hash was found */
    unsigned int memcmp_after_col : 1;
};

/**
 * @struct ezrc_hashmap_elm
 * @brief  struct holding an element stored in the hashmap
 */
struct ezrc_hashmap_elm
{
    /** Points to next element on the row this element is on, or NULL */
    struct ezrc_linked_list_node ll;
    /** Keep hash stored so that the hashmap can resize to optimize its load factor without recalculating the hash */
    uint64_t hash;
    /** key can be stored as a reference, or a copied value. when it is copied, we need to keep the length */
    union {
        void *key_ptr;
        size_t key_len;
    };
    /** if the value is stored as a reference, this pointer is set, otherwise it will be NULL */
    void *val_ptr;
    /** depending on the config this pointer can hold a copy of the key, the value, both (key first, then val), or neither. this member makes the struct's size variable. */
    void *key_val;
};

/**
 * @struct ezrc_hashmap_row
 * @brief  struct representing a row in the hashmap.
 */
struct ezrc_hashmap_row
{
    /** the row is a linked list. points to ``struct ezrc_hashmap_elm`` */
    struct ezrc_linked_list_head ll_elm_head;
};

/**
 * @struct ezrc_hashmap
 * @brief  top hashmap struct
 */
struct ezrc_hashmap
{
#ifdef EZRC_HAS_PTHREAD
    int threadsafe;
#endif
    /** holds allocator to use */
    struct ezrc_allocator allocator;
    /** holds config of the map */
    struct ezrc_hashmap_cfg cfg;
    /** keeps track of when the next load factor check is due. If the load factor violates the limits of the config, the hashmap is resized. */
    unsigned int cur_size_check_itv : 8;
    /** the number of elements */
    size_t filling;
    /** the number of rows */
    size_t nrof_rows;
    /** array holding the rows */
    struct ezrc_hashmap_row *rows;
};

/**
 * @brief wrapper around the stdlib strlen function made for use in the hashmap configuration.
 * @param str pointer to pass to the strlen function. will be cast to ``const char *``
 */
size_t ezrc_hashmap_str_len (const void *str);

/**
 * @brief initialize memory for usage as a hashmap
 * @param allocator allocator to use in the hashmap
 * @param map       pointer to the memory that will be the hashmap.
 * @param cfg       config to use, will be copied in the hashmap structure.
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_init (struct ezrc_allocator allocator, struct ezrc_hashmap *map, struct ezrc_hashmap_cfg cfg);

/**
 * @brief put an element in the map
 * @param map pointer to the map
 * @param key key to put the value under
 * @param val value to input
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_put (struct ezrc_hashmap *map, void *key, void *val);

/**
 * @brief get an element from the map
 * @param map     map to use
 * @param key     key to look for
 * @param opt_err optional pointer to an ezrc_return struct that will give more info after
 *                the search whether something went wrong or if the value was not found
 * @returns pointer to the found value if the key was succesfully found, NULL otherwise
 */
void *ezrc_hashmap_get (const struct ezrc_hashmap *map, const void *key, struct ezrc_return *opt_err);

/**
 * @brief remove an element from the map
 * @param map pointer to the map to delete from
 * @param key key to delete
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_del (struct ezrc_hashmap *map, void *key);

/**
 * @brief deinitialize the hashmap. will free everything allocated by the hashmap.
 * @param map pointer to the map to deinitialize
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_deinit (struct ezrc_hashmap *map);

#ifndef EZRC_HASHMAP_HASH_SEED
/**
 * @brief initial seed used in the hashing function when hashing the keys. Can be overriden by defining this before including the header.
 */
#define EZRC_HASHMAP_HASH_SEED 0
#endif

/**
 * @brief default configuration for the hashmap.
 * @param ks size of the keys
 * @param vs size of the value
 */
#define EZRC_DEFAULT_HASHMAP_CFG(ks,vs) \
(struct ezrc_hashmap_cfg){              \
    .initial_nrof_rows = 8,             \
    .sizeof_key = (ks),                 \
    .get_key_len = NULL,                \
    .sizeof_val = (vs),                 \
    .get_val_len = NULL,                \
    .max_load_factor = 2.0,             \
    .min_load_factor = .25,             \
    .size_check_intv = 4,               \
    .store_key = 0,                     \
    .store_val = 0,                     \
    .allow_resize = 1,                  \
    .allow_overwrite = 0,               \
    .memcmp_after_col = 0,              \
}

/**
 * @brief default configuration for a hashmap with strings as keys
 * @param vs size of the values in the map
 */
#define EZRC_DEFAULT_HASHMAP_STR_KEY_CFG(vs)         \
(struct ezrc_hashmap_cfg){                           \
    .initial_nrof_rows = 8,                          \
    .sizeof_key = 0,                                 \
    .get_key_len = ezrc_hashmap_str_len,             \
    .sizeof_val = (vs),                              \
    .get_val_len = NULL,                             \
    .max_load_factor = 2.0,                          \
    .min_load_factor = .25,                          \
    .size_check_intv = 3,                            \
    .store_key = 0,                                  \
    .store_val = 0,                                  \
    .allow_resize = 1,                               \
    .allow_overwrite = 0,                            \
    .memcmp_after_col = 0,                           \
}

/**
 * @brief helper macro to input values directly into a hashmap. Note that the map needs to be configured to store the values!
 * @param type type to insert
 * @param map  map to insert into
 * @param rval value to insert
 * @param ret  will contain the return code of the operation
 */
#define ezrc_hashmap_put_rval(type, map, key, rval, ret) do { \
    type val = rval;                                          \
    ret = ezrc_hashmap_put ((map), key, &val);                \
} while (0)

#ifdef EZRC_HAS_PTHREAD

#include <pthread.h>

/**
 * @struct ezrc_hashmap_ts
 * @brief  top hashmap struct -- the threadsafe version
 */
struct ezrc_hashmap_ts
{
#ifdef EZRC_HAS_PTHREAD
    int threadsafe;
#endif
    /** holds allocator to use */
    struct ezrc_allocator allocator;
    /** holds config of the map */
    struct ezrc_hashmap_cfg cfg;
    /** keeps track of when the next load factor check is due. If the load factor violates the limits of the config, the hashmap is resized. */
    unsigned int cur_size_check_itv : 8;
    /** the number of elements */
    size_t filling;
    /** the number of rows */
    size_t nrof_rows;
    /** array holding the rows */
    struct ezrc_hashmap_row *rows;
    pthread_rwlock_t rwlock;
};

/**
 * @brief initialize memory for usage as a threadsafe hashmap
 * @param allocator allocator to use in the hashmap
 * @param map       pointer to the memory that will be the threadsafe hashmap.
 * @param cfg       config to use, will be copied in the hashmap structure.
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_init_ts (struct ezrc_allocator allocator, struct ezrc_hashmap_ts *map, struct ezrc_hashmap_cfg cfg);

/**
 * @brief Manually aquire a read lock. multiple threads can hold a read lock at the same time.
 * @param hashmap threadsafe hashmap
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_get_rd_lock (struct ezrc_hashmap_ts *hashmap);

/**
 * @brief Manually aquire a write lock. Only one write lock can exist. No read locks exist when a write lock exists.
 * @param hashmap threadsafe hashmap
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_get_wr_lock (struct ezrc_hashmap_ts *hashmap);

/**
 * @brief Manually release a read or write lock
 * @param hashmap threadsafe hashmap
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_hashmap_release_lock (struct ezrc_hashmap_ts *hashmap);

#endif

#endif
