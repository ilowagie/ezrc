/**
 * @headerfile ezrc/containers/linked_list.h
 * @brief      Contains the base api for the linked list container
 */
#ifndef EZRC_LINKED_LIST_H
#define EZRC_LINKED_LIST_H

#include "ezrc/error_codes.h"
#include "ezrc/containers/iterator.h"

/**
 * @struct ezrc_linked_list_node
 * @brief  Node of a linked list. Place this struct in a struct that you want to make a linked list out of.
 *         Then use ezrc_struct_of to get the element's data.
 */
struct ezrc_linked_list_node
{
    struct ezrc_linked_list_node *next;
};

/**
 * @struct ezrc_linked_list_head
 * @brief  struct to keep track of a linked list.
 */
struct ezrc_linked_list_head
{
#ifdef EZRC_HAS_PTHREAD
    int threadsafe;
#endif
    struct ezrc_linked_list_node *first;
    struct ezrc_iterable iterable;
};

/**
 * @brief initialize the data of a linked list head.
 * @param list pointer to allocated memory to initialize a linked list head in
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_linked_list (struct ezrc_linked_list_head *list);

/**
 * @brief append an element to the back of a linked list
 * @param head the head of the linked list to append to
 * @param node node to append
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_linked_list_append_back (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node);

/**
 * @brief append an element to the front of a linked list
 * @param head the head of the linked list to append to
 * @param node node to append
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_linked_list_append_front (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node);

/**
 * @brief pop an element from the back of a linked list
 * @param head head of the linked list to take an element from
 * @returns a pointer to the element that was just removed. If the list was empty, this pointer will be NULL
 */
struct ezrc_linked_list_node *ezrc_linked_list_pop_back (struct ezrc_linked_list_head *head);

/**
 * @brief pop an element from the back of a linked list
 * @param head head of the linked list to take an element from
 * @returns a pointer to the element that was just removed. If the list was empty, this pointer will be NULL
 */
struct ezrc_linked_list_node *ezrc_linked_list_pop_front (struct ezrc_linked_list_head *head);

/**
 * @brief remove a node from anywhere in the linked list
 * @param head head of the linked list to take an element from
 * @param node node to remove
 * @returns EZRC_OK if the node was removed, EZRC_ERR_NOT_FOUND otherwise
 */
struct ezrc_return ezrc_linked_list_remove (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node);

/**
 * @brief get the last node of the linked list
 * @param head head of the linked list to get the last node from
 * @returns a pointer to the last node or NULL if list was empty
 */
struct ezrc_linked_list_node *ezrc_linked_list_last (struct ezrc_linked_list_head *head);

#ifdef EZRC_HAS_PTHREAD

#include <pthread.h>

/**
 * @struct ezrc_linked_list_head_ts
 * @brief  struct to keep track of a threadsafe linked list.
 */
struct ezrc_linked_list_head_ts
{
    int threadsafe;
    struct ezrc_linked_list_node *first;
    struct ezrc_iterable iterable;
    pthread_rwlock_t rwlock;
};

/**
 * @brief initialize the data of a threadsafe linked list head.
 * @param list pointer to allocated memory to initialize a linked list head in
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_linked_list_ts (struct ezrc_linked_list_head_ts *list);

/**
 * @brief Manually aquire a read lock. multiple threads can hold a read lock at the same time.
 * @param head threadsafe linked_list
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_linked_list_get_rd_lock (struct ezrc_linked_list_head_ts *head);

/**
 * @brief Manually aquire a write lock. Only one write lock can exist. No read locks exist when a write lock exists.
 * @param head threadsafe linked_list
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_linked_list_get_wr_lock (struct ezrc_linked_list_head_ts *head);

/**
 * @brief Manually release a read or write lock
 * @param head threadsafe linked_list
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_linked_list_release_lock (struct ezrc_linked_list_head_ts *head);

#endif

#endif
