/**
 * @headerfile ezrc/containers/stack.h
 * @brief      Contains everything for the ezrc_stack container
 */
#ifndef EZRC_STACK_H
#define EZRC_STACK_H

#include <stdlib.h>

#include "ezrc/base_structs.h"
#include "ezrc/error_codes.h"
#include "ezrc/containers/iterator.h"

/**
 * @struct ezrc_stack
 * @brief  a stack is a dynamic array that allows you to push and pop items
 */
struct ezrc_stack
{
#ifdef EZRC_HAS_PTHREAD
    int                     threadsafe;
#endif
    size_t                  len;
    size_t                  cap;
    struct ezrc_allocator   allocator;
    void                  **elms;
    /** callback to function to free elements in the stack */
    void (*free_elm) (struct ezrc_allocator allocator, void *ptr);
    struct ezrc_iterable iterable;
};

/**
 * @brief initialize a stack
 * @param stack     stack to initialize
 * @param init_cap  initial capacity of the stack
 * @param allocator allocator to use to allocate the stack and dynamic array
 * @param free_elm  function to call on each element when the stack is freed
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_stack (struct ezrc_stack     *stack,
                                    const size_t           init_cap,
                                    struct ezrc_allocator  allocator,
                                    void (*free_elm) (struct ezrc_allocator, void *));

/**
 * @brief set the size of the stack (expressed in number of elements)
 * @param stack stack to resize
 * @param size  number of elements
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_stack_set_size (struct ezrc_stack *stack, const size_t size);

/**
 * @brief push an element on top of the stack. the stack will take ownership of this element!
 * @param stack stack to push element on
 * @param elm   element to push
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_stack_push (struct ezrc_stack *stack, void *elm);

/**
 * @brief pop the top element of the stack. You will take back ownership of this element.
 * @param stack stack to pop from
 * @returns pointer to the element on success. NULL if there are no elements.
 */
void *ezrc_stack_pop (struct ezrc_stack *stack);

/**
 * @brief clear and free all elements in the stack.
 * @param stack stack to clear
 */
struct ezrc_return ezrc_stack_clear (struct ezrc_stack *stack);

/**
 * @brief cleanup all internal allocations in the stack
 * @param stack pointer to the stack to cleanup
 */
struct ezrc_return ezrc_cleanup_stack (struct ezrc_stack *stack);

#ifdef EZRC_HAS_PTHREAD

#include <pthread.h>

/**
 * @struct ezrc_stack_ts
 * @brief  threadsafe version of ezrc_stack
 */
struct ezrc_stack_ts
{
    int                     threadsafe;
    size_t                  len;
    size_t                  cap;
    struct ezrc_allocator   allocator;
    void                  **elms;
    void (*free_elm) (struct ezrc_allocator allocator, void *ptr);
    struct ezrc_iterable    iterable;
    pthread_rwlock_t        rwlock;
};

/**
 * @brief initialize a threadsafe stack
 * @param stack     stack to initialize
 * @param init_cap  initial capacity of the stack
 * @param allocator allocator to use to allocate the stack and dynamic array
 * @param free_elm  function to call on each element when the stack is freed
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_stack_ts (struct ezrc_stack_ts  *stack,
                                       const size_t           init_cap,
                                       struct ezrc_allocator  allocator,
                                       void (*free_elm) (struct ezrc_allocator, void *));

/**
 * @brief Manually aquire a read lock. multiple threads can hold a read lock at the same time.
 * @param stack threadsafe stack
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_stack_get_rd_lock (struct ezrc_stack_ts *stack);

/**
 * @brief Manually aquire a write lock. Only one write lock can exist. No read locks exist when a write lock exists.
 * @param stack threadsafe stack
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_stack_get_wr_lock (struct ezrc_stack_ts *stack);

/**
 * @brief Manually release a read or write lock
 * @param stack threadsafe stack
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_stack_release_lock (struct ezrc_stack_ts *stack);

#endif

#endif
