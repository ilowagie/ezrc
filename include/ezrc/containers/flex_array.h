/**
 * @headerfile ezrc/containers/flex_array.h
 * @brief      contains the flex_array structure for creating dynamic arrays in C
 */
#ifndef EZRC_FLEX_ARRAY_H
#define EZRC_FLEX_ARRAY_H

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>

#include "ezrc/base_structs.h"
#include "ezrc/error_codes.h"
#include "ezrc/containers/iterator.h"

/**
 * @struct ezrc_flex_array
 * @brief  The ezrc_flex_array struct allows you to create dynamic arrays.
 *         Note: It does this with some pointer magic.
 *               The pro is that all elements are placed next to each other in memory,
 *               The con is that this container might result in crashes as there are less sanity checks as in the other containers
 */
struct ezrc_flex_array
{
#ifdef EZRC_HAS_PTHREAD
    int    threadsafe;
#endif
    size_t len;
    size_t cap;
    size_t elm_size;

    struct ezrc_allocator  allocator;
    struct ezrc_iterable   iterable;
    void                  *array;
};

/**
 * @brief Initialize the flex array with an initial capacity
 * @param array     pointer to memory where the flex_array can be initialized
 * @param allocator the allocator to use for internal allocations
 * @param init_cap  the initial capacity of the array
 * @param elm_size  the size of each element. This is needed for the pointer magic
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_flex_array (struct ezrc_flex_array *array,
                                         struct ezrc_allocator allocator,
                                         const size_t init_cap,
                                         const size_t elm_size);

/**
 * @brief check if there's room for one more element, if there isn't, allocate more room
 * @param array a valid pointer to a flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_flex_array_room_for_one_more (struct ezrc_flex_array *array);

/**
 * @brief unsafe version of the ezrc_flex_array_room_for_one_more function. Avoid calling this function.
 * @param array a valid pointer to a flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_flex_array_room_for_one_more__unsafe (struct ezrc_flex_array *array);

/**
 * @brief append an element to the flex array without checking any locks. Use when you want to have multiple appends close to each other
 * @param type    type of the elements in the flex array
 * @param f_array a valid pointer to the flex array
 * @param elm     the element to insert
 */
#define ezrc_flex_array_append__unsafe(type, f_array, elm) do                                                                    \
    {                                                                                                                            \
        type internal_elm = elm;                                                                                                 \
        struct ezrc_return info = ezrc_flex_array_room_for_one_more__unsafe ((f_array));                                         \
        if (info.code == EZRC_OK)                                                                                                \
        {                                                                                                                        \
            memcpy (((uint8_t *)(f_array)->array + ((f_array)->len * (f_array)->elm_size)), &internal_elm, (f_array)->elm_size); \
            (f_array)->len++;                                                                                                    \
        }                                                                                                                        \
    } while (0)

/**
 * @brief set the element on a given index to a new value without any lock checks
 * @param type    type of the elements in the array
 * @param f_array pointer to a valid flex_array
 * @param elm     the element to set on the given index
 * @param idx     index to change
 */
#define ezrc_flex_array_set__unsafe(type, f_array, elm, idx) do                                                     \
    {                                                                                                               \
        type internal_elm = elm;                                                                                    \
        memcpy (((uint8_t *)((f_array)->array) + (idx * (f_array)->elm_size)), &internal_elm, (f_array)->elm_size); \
    } while (0)

#ifdef EZRC_HAS_PTHREAD

/**
 * @brief append an element to the flex array
 * @param type    type of the elements in the flex array
 * @param f_array a valid pointer to the flex array
 * @param elm     the element to insert
 */
#define ezrc_flex_array_append(type, f_array, elm) do                                     \
    {                                                                                     \
        if ((f_array)->threadsafe)                                                        \
        {                                                                                 \
            struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)(f_array); \
            if (pthread_rwlock_wrlock (&array_ts->rwlock)) break;                         \
        }                                                                                 \
        ezrc_flex_array_append__unsafe (type, f_array, elm);                              \
        if ((f_array)->threadsafe)                                                        \
        {                                                                                 \
            struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)(f_array); \
            pthread_rwlock_unlock (&array_ts->rwlock);                                    \
        }                                                                                 \
    } while (0)

/**
 * @brief set the element on a given index to a new value
 * @param type    type of the elements in the array
 * @param f_array pointer to a valid flex_array
 * @param elm     the element to set on the given index
 * @param idx     index to change
 */
#define ezrc_flex_array_set(type, f_array, elm, idx) do                                   \
    {                                                                                     \
        if ((f_array)->threadsafe)                                                        \
        {                                                                                 \
            struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)(f_array); \
            if (pthread_rwlock_wrlock (&array_ts->rwlock)) break;                         \
        }                                                                                 \
        ezrc_flex_array_set__unsafe(type, f_array, elm, idx);                             \
        if ((f_array)->threadsafe)                                                        \
        {                                                                                 \
            struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)(f_array); \
            pthread_rwlock_unlock (&array_ts->rwlock);                                    \
        }                                                                                 \
    } while (0)

#else

/**
 * @brief append an element to the flex array
 * @param type    type of the elements in the flex array
 * @param f_array a valid pointer to the flex array
 * @param elm     the element to insert
 */
#define ezrc_flex_array_append ezrc_flex_array_append__unsafe

/**
 * @brief set the element on a given index to a new value
 * @param type    type of the elements in the array
 * @param f_array pointer to a valid flex_array
 * @param elm     the element to set on the given index
 * @param idx     index to change
 */
#define ezrc_flex_array_set ezrc_flex_array_set__unsafe

#endif

/**
 * @brief internal function for getting a pointer to the part of the array that is on the given index
 * @param array a valid pointer to a flex array
 * @param idx   the index in the array
 * @returns NULL on failure, otherwise a pointer to a part of the array
 */
void *ezrc_flex_array_get_elm__internal (const struct ezrc_flex_array *array, const size_t idx);

/**
 * @brief macro around `ezrc_flex_array_get_elm__internal` to get an element on the given offset
 * @param type  type of the elements in the array
 * @param array pointer to the array
 * @param idx   index to get
 */
#define ezrc_flex_array_get(type, array, idx) \
    (*((type *)ezrc_flex_array_get_elm__internal (array, idx)))

/**
 * @brief clean up internal allocations of the flex array
 * @param array     pointer to the flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_cleanup_flex_array (struct ezrc_flex_array *array);

/**
 * @brief If you have a flex_array of chars, this function will allow you to append a C string to it
 *        It takes care of the \0 string termination for you
 * @param flex_str a pointer to a flex array of chars
 * @param str      a C style string ending on \0
 * @returns an ezrc_return function
 */
struct ezrc_return ezrc_flex_string_concat (struct ezrc_flex_array *flex_str, const char *str);

#ifdef EZRC_HAS_PTHREAD

#include <pthread.h>

/**
 * @struct ezrc_flex_array_ts
 * @brief  The threadsafe version of the ezrc_flex_array struct that allows you to create dynamic arrays.
 *         Note: It does this with some pointer magic.
 *               The pro is that all elements are placed next to each other in memory,
 *               The con is that this container might result in crashes as there are less sanity checks as in the other containers
 */
struct ezrc_flex_array_ts
{
    int    threadsafe;
    size_t len;
    size_t cap;
    size_t elm_size;

    struct ezrc_allocator  allocator;
    struct ezrc_iterable   iterable;
    void                  *array;
    pthread_rwlock_t       rwlock;
};

/**
 * @brief Initialize the threadsafe flex array with an initial capacity
 * @param array     pointer to memory where the flex_array can be initialized
 * @param allocator the allocator to use for internal allocations
 * @param init_cap  the initial capacity of the array
 * @param elm_size  the size of each element. This is needed for the pointer magic
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_init_flex_array_ts (struct ezrc_flex_array_ts *array,
                                            struct ezrc_allocator allocator,
                                            const size_t init_cap,
                                            const size_t elm_size);

/**
 * @brief Manually aquire a read lock. multiple threads can hold a read lock at the same time.
 * @param array threadsafe flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_array_get_rd_lock (struct ezrc_flex_array_ts *array);

/**
 * @brief Manually aquire a write lock. Only one write lock can exist. No read locks exist when a write lock exists.
 * @param array threadsafe flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_array_get_wr_lock (struct ezrc_flex_array_ts *array);

/**
 * @brief Manually release a read or write lock
 * @param array threadsafe flex array
 * @returns an ezrc_return struct
 */
struct ezrc_return ezrc_array_release_lock (struct ezrc_flex_array_ts *array);

#endif

#endif
