/**
 * @headerfile ezrc/iterator.h
 * @brief      Iterator API: a way to manipulate arrays
 */

#ifndef EZRC_ITERATOR_H
#define EZRC_ITERATOR_H

#include "ezrc/base_structs.h"

struct ezrc_iterable;

/**
 * @struct ezrc_iterator
 * @brief  struct holding everything needed to iterate over something iterable.
 *         Each iteration is represented by an `ezrc_iterator` struct.
 *         A single iterable struct can have multiple iterators associated with it.
 */
struct ezrc_iterator
{
    struct ezrc_allocator allocator;
    /**
     * use this pointer in combination with the `ezrc_struct_of` macro utility
     * to get a pointer back to the struct your iterating over
     */
    const struct ezrc_iterable *iterable;
    /**
     * holds the context of this specific iteration
     */
    void *ctx;

    int *is_new;
};

/**
 * @struct ezrc_iterable
 * @brief  struct holding the implementations that form the base api for iterating over something
 */
struct ezrc_iterable
{
    /**
     * function getting the next element using the iterator struct. returns NULL if there are no elements left.
     */
    void *(*next)     (struct ezrc_iterator iterator);
    /**
     * Holds a pointer to the function for allocating and initializing an iterator context.
     */
    void *(*init_ctx) (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable);
    /**
     * Holds a pointer to the function for freeing the iterator context.
     * Is called when `ezrc_iterator_end` is called, or when the iterator has encountered the last element
     */
    void (*free_ctx) (struct ezrc_allocator allocator, void *iterator_ctx);
    /**
     * Optional function for checking if the iterator has reached the end without consuming the last element
     */
    int (*is_at_end) (struct ezrc_iterator iterator);
};

/**
 * @brief get an iterator for the given iterable
 * @param allocator allocator to pass to the init function for initializing the iterator's context
 * @param iterable  a pointer to the iterable struct inside the struct you want to iterate over.
 *                  Note that, for the best usability, the iterable struct should NOT be a pointer inside your struct,
 *                  and should thus be passed to this function with dereferencing the field.
 * @returns an iterator struct that can be used for iterating
 */
struct ezrc_iterator ezrc_iterator_get (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable);

/**
 * @brief get the next element and advance the iterator. If there are no elements left, returns NULL.
 * @param iterator the iterator to use
 * @returns NULL if no elements left, otherwise a valid pointer to the next element of the iterator
 */
void *ezrc_iterator_next (struct ezrc_iterator iterator);

/**
 * @brief way to check if the iterator has just started
 * @param iterator iterator to check
 * @returns 1 if next has never been called on the iterator or if the first element has been taken, 0 otherwise
 */
int ezrc_iterator_is_new (struct ezrc_iterator iterator);

/**
 * @brief way to check if the iterator has reached is end, i.e. calling next would return NULL
 * @param iterator iterator to check
 * @returns 1 if the `is_at_end` function is implemented for the iterator and the iterator has reached its end.
 *          Note that a value of 0 can also mean that this functionality hasn't been implemented for the given iterator.
 */
int ezrc_iterator_is_at_end (struct ezrc_iterator iterator);

/**
 * @brief end the iterator (prematurely). will free the iterator's context. Note that this function is
 *        automatically called when the iterator reaches the last element.
 * @param iterator iterator to end
 */
void ezrc_iterator_end (struct ezrc_iterator iterator);

/**
 * @brief iterate over all elements in a container. can be any container of the EZRC library
 * @param allocator allocator for allocating the iterator context
 * @param iterable  pointer to the iterable field to use as the iterator interface
 * @param elm       iterator variable name that will be updated and can be used in the loop body
 */
#define ezrc_foreach(allocator, iterable, elm) \
    for (struct ezrc_iterator iter = ezrc_iterator_get (allocator, iterable); (elm = ezrc_iterator_next (iter));)

/**
 * @brief is 1 if we are at the first iteration in the foreach loop (only works inside body of ezrc_foreach)
 */
#define ezrc_foreach_is_first (ezrc_iterator_is_new (iter))

/**
 * @brief is 1 if we are at the last iteration in the foreach loop (only works inside body of ezrc_foreach)
 */
#define ezrc_foreach_is_last (ezrc_iterator_is_at_end (iter))

/**
 * @brief make sure to call this if you exit the foreach loop prematurely e.g. with a return
 */
#define ezrc_foreach_exit_loop (ezrc_iterator_end (iter))

/**
 * @struct ezrc_map_iterator_ctx
 * @brief iterator context used for a mapping iterator
 */
struct ezrc_map_iterator_ctx
{
    struct ezrc_iterator intern_iter;
    void *(*map_f) (void *input, void *extern_args);
    void *extern_args;
};

/**
 * @brief map a function on an iterator. Each time next is called on this new iterator,
 *        you will get the return value of map_f applied on the next value of the internal iterator.
 * @param iterator    iterator to apply the function to
 * @param map_f       mapping function taking an address to an object returning an object.
 *                    The input is checked to not be NULL before applying the mapping function.
 *                    The second input of the map function can be used for external arguments that are passed into this function.
 * @param extern_args external arguments that are passed into each call of the mapping function
 * @returns a new iterator
 */
struct ezrc_iterator ezrc_iterator_map (struct ezrc_iterator iterator, void *(*map_f) (void *, void *), void *extern_args);

/**
 * @struct ezrc_filter_iterator_ctx
 * @brief iterator context used in the filter iterator
 */
struct ezrc_filter_iterator_ctx
{
    struct ezrc_iterator intern_iter;
    int (*filter_f) (void *input, void *extern_args);
    void *extern_args;
};

/**
 * @brief create a new iterator that will filter out elements from an other iterator.
 * @param iterator    iterator to filter
 * @param filter_f    filter function to use. this is a function that takes a `void *` and returns an int.
 *                    the `void *` pointers passed into the function will be the elements of the given iterator.
 *                    if the filter function returns non zero, the element will be returned, otherwise, the next element will be tested.
 *                    The filter function takes another `void *` which will point to the passed `extern_args` on every call.
 * @param extern_args external arguments that are passed into each call of the filter function
 * @returns a new iterator
 */
struct ezrc_iterator ezrc_iterator_filter (struct ezrc_iterator iterator, int (*filter_f) (void *, void *), void *extern_args);

/**
 * @brief fold an iterator into a single value
 * @param iterator    the iterator to process
 * @param accumulator a pointer to the accumulator value. Will be passed on to the fold function for every value.
 *                    Note: you need to initialize this value before calling fold with it.
 * @param fold_f      the folding function. it takes 3 arguments. the first one is the pointer to the accumulator.
 *                    the second one is a pointer to the element from the underlying iterator.
 *                    the third one is a pointer to the `extern_args` which is a custom list of arguments you can pass to it.
 * @param extern_args external arguments that are passed into each call of the filter function
 * @returns when the iterator is finished. If the iterator is infinite, this function never returns.
 */
void ezrc_iterator_fold (struct ezrc_iterator iterator, void *accumulator, void (*fold_f) (void *acc, void *elm, void *args), void *extern_args);

#endif
