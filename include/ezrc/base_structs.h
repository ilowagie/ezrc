/**
 * @headerfile ezrc/base_structs.h
 * @brief      Contains all the basic structs needed to use the rest of the library
 */

#ifndef EZRC_BASE_STRUCTS_H
#define EZRC_BASE_STRUCTS_H

#include <stdlib.h>

enum ezrc_alloc_flags
{
    EZRC_ALLOC_FLAG_DEFAULT    = 0,
    EZRC_ALLOC_FLAG_NO_REALLOC = 1,
};

/**
 * @struct ezrc_allocator
 * @brief  Allows you to pass a custom allocator to functions
 */
struct ezrc_allocator
{
    /**
     * alloc CANNOT be NULL. Every allocator must have an alloc function. The other functionality is optional.
     */
    void *(*alloc) (const size_t size, void *data);
    void *(*realloc) (void *ptr, const size_t new_size, void *data);
    void (*free) (void *ptr, void *data);
    /**
     * data can be NULL, or it can be a context that is passed to all the above functions
     */
    void *data;
};

#endif
