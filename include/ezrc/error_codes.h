/**
 * @headerfile ezrc/error_codes.h
 * @brief      contains all the necessary things for error handling in ezrc
 */

#ifndef EZRC_ERROR_CODES_H
#define EZRC_ERROR_CODES_H

#include <stdlib.h>

/**
 * @enum  ezrc_error_code
 * @brief all the potential error codes that can be outputed by ezrc functions
 */
enum ezrc_error_code {
    EZRC_OK,
    EZRC_ERR_ALLOC_FAIL,
    EZRC_ERR_NULL_PTR_ACCESS,
    EZRC_ERR_STD_STR,
    EZRC_ERR_FILE_OP,
    EZRC_ERR_INTERNAL_CONTRADICTION,
    EZRC_ERR_UNSUPPORTED_ACTION,
    EZRC_ERR_NOT_FOUND,
    EZRC_ERR_BAD_MTX,
    EZRC_ERR_UNIQUE_VIOLATION,
    EZRC_ERR_MAX_SIZE_REACHED,
    EZRC_ERR_UNKNOWN,
};

/**
 * @brief contains all the strings with verbose error descriptions. Index with ezrc_error_code
 */
extern const char *ezrc_error_code_strings_c[EZRC_ERR_UNKNOWN];

/**
 * @struct ezrc_return
 * @brief struct containing info about failures (or success)
 */
struct ezrc_return
{
    enum ezrc_error_code code;
    char *file;
    int   line;
};

/**
 * @brief convert an error code into an error string
 * @param code code to convert. boundary checking will be done.
 * @returns a pointer to a static string, or NULL when the error code is not known.
 */
static inline const char *ezrc_err_str (const enum ezrc_error_code code)
{
    if (code >= EZRC_ERR_UNKNOWN) return NULL;
    else return ezrc_error_code_strings_c[code];
}

/**
 * @brief macro to get the return struct for the given code
 * @param c code to set
 */
#define EZRC_RETURN(c) (struct ezrc_return){ .code = (c), .file = __FILE__, .line = __LINE__ }

/**
 * @brief wrapping function to set an optional error code
 * @param info pointer to the return struct. if NULL, no code will be set
 * @param ret  code to set
 */
static inline void ezrc_set_opt_error (struct ezrc_return *info, const struct ezrc_return ret)
{
    if (info) *info = ret;
}

#endif
