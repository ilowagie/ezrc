/**
 * @headerfile ezrc/macro_util.h
 * @brief      Contains some basic macros to make life easier
 */

#ifndef EZRC_MACRO_UTIL_H
#define EZRC_MACRO_UTIL_H

#include <stddef.h>
#include <stdint.h>

/**
 * @brief helper for EZRC_STRINGIFY
 */
#define EZRC_STRINGIFY_S0(x) #x

/**
 * @brief convert macro value to string
 */
#define EZRC_STRINGIFY(x) EZRC_STRINGIFY_S0(x)

/**
 * @brief get the maximum of 2 variables. if equal, first element is returned.
 */
#define EZRC_MAX(a, b) ((a < b) ? b : a)

/**
 * @brief use pointer magic to get a pointer to the struct that has the given pointer as a member
 * @param ptr         a pointer located in the struct. If it is NULL, the function shall return NULL as well
 * @param struct_type name of the struct
 * @param member_name the name that the given data has inside the struct
 */
#define ezrc_struct_of(ptr, struct_type, member_name) \
    ((ptr) ? (struct_type *)((uint8_t *)(ptr) - offsetof (struct_type, member_name)) : NULL)

#endif
