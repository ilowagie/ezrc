/**
 * @headerfile ezrc/version.h
 * @brief      Contains the 3 version numbers and the version string macro
 */

#ifndef EZRC_VERSION_H
#define EZRC_VERSION_H

#include "ezrc/macro_util.h"

#define EZRC_VERSION_MAJOR 0
#define EZRC_VERSION_MINOR 1
#define EZRC_VERSION_PATCH 3

#define EZRC_VERSION_STRING \
    ""  EZRC_STRINGIFY (EZRC_VERSION_MAJOR) \
    "." EZRC_STRINGIFY (EZRC_VERSION_MINOR) \
    "." EZRC_STRINGIFY (EZRC_VERSION_PATCH)

#endif
