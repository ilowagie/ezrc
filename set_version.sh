#!/bin/sh

show_help() {
    cat << EOF
Usage: ${0##*/} [-h] [-m MAJOR] [-i MINOR] [-p PATCH]
Use script to set the version values to the correct values.
Will make sure that the version is correctly set in all files.
Only the given versions will be updated, the other ones will remain the same.

    -h       show this help and exit
    -v       print extra info during script execution
    -m MAJOR new major version to set
    -i MINOR new minor version to set
    -p PATCH new patch version to set
EOF
}

OPTIND=1

major=""
minor=""
patch=""
version_string=""
verbose=0

function log_debug() {
    [ $verbose -eq 1 ] && echo $@
}

while getopts "h?m:i:p:v" opt
do
    case "$opt" in
        h|\?)
            show_help
            exit 0
            ;;
        v)
            verbose=1
            ;;
        m)
            major=$OPTARG
            ;;
        i)
            minor=$OPTARG
            ;;
        p)
            patch=$OPTARG
            ;;
        *)
            show_help >&2
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))"

# get version numbers that are unset
if [ ! "$major" ]
then
    log_debug "major not set. searching old value in 'include/ezrc/version.h'..."
    version_match=$(grep -o -m 1 "EZRC_VERSION_MAJOR\s\+[0-9]\+" include/ezrc/version.h)
    major=${version_match##EZRC_VERSION_MAJOR[+[:blank:]]}
    log_debug "...found major version ${major}"
fi
if [ ! "$minor" ]
then
    log_debug "minor not set. searching old value in 'include/ezrc/version.h'..."
    version_match=$(grep -o -m 1 "EZRC_VERSION_MINOR\s\+[0-9]\+" include/ezrc/version.h)
    minor=${version_match##EZRC_VERSION_MINOR[+[:blank:]]}
    log_debug "...found minor version ${minor}"
fi
if [ ! "$patch" ]
then
    log_debug "patch not set. searching old value in 'include/ezrc/version.h'..."
    version_match=$(grep -o -m 1 "EZRC_VERSION_PATCH\s\+[0-9]\+" include/ezrc/version.h)
    patch=${version_match##EZRC_VERSION_PATCH[+[:blank:]]}
    log_debug "...found patch version ${patch}"
fi

# set full version string
version_string="${major}.${minor}.${patch}"
echo "full version string to set: ${version_string}"
version_pattern="[0-9]+.[0-9]+.[0-9]+"

# search and replace everywhere a version is defined
log_debug "search and replace in 'meson.build'"
sed -ri "s/(version:\s+)'${version_pattern}'/\1'${version_string}'/"         meson.build
sed -ri "s/(ezrc_version\s+=\s+)'${version_pattern}'/\1'${version_string}'/" meson.build

log_debug "search and replace in 'include/ezrc/version.h'"
sed -ri "s/(#define\s+EZRC_VERSION_MAJOR\s)[0-9]+/\1${major}/" include/ezrc/version.h
sed -ri "s/(#define\s+EZRC_VERSION_MINOR\s)[0-9]+/\1${minor}/" include/ezrc/version.h
sed -ri "s/(#define\s+EZRC_VERSION_PATCH\s)[0-9]+/\1${patch}/" include/ezrc/version.h

log_debug "search and replace in 'doc/Doxyfile'"
sed -ri "s/(PROJECT_NUMBER\s+=\s+)${version_pattern}/\1${version_string}/" doc/Doxyfile

log_debug "search and replace in 'doc/src/conf.py'"
sed -ri "s/(release\s+=\s+)'${version_pattern}'/\1'${version_string}'/" doc/src/conf.py
