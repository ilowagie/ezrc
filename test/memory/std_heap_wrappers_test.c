#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include <criterion/criterion.h>
#include <criterion/theories.h>
#include <criterion/new/assert.h>

#include "ezrc/macro_util.h"
#include "ezrc/memory/allocator.h"

#include "leak_check_helpers.h"

#define BUF_MAX LINE_MAX

TheoryDataPoints (std_heap_wrapper, alloc_use_free) = {
    DataPoints(size_t, 2, 64, 1024, BUF_MAX),
};
Theory((size_t alloc_size), std_heap_wrapper, alloc_use_free)
{
    cr_assume (alloc_size > 1);
    cr_assume (alloc_size <= BUF_MAX);

    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "std_heap_wrapper_alloc_use_free");

    char test_string[BUF_MAX];

    for (size_t i = 0; i < alloc_size-1; i++) test_string[i] = (char)((i + 48) % 126);
    test_string[alloc_size-1] = '\0';

    char *string = ezrc_allocator_alloc (test_allocator, alloc_size);
    cr_assert(ne(ptr, string, NULL));
    for (size_t i = 0; i < alloc_size-1; i++) string[i] = (char)((i + 48) % 126);
    string[alloc_size-1] = '\0';

    cr_assert(eq (str, string, test_string));

    ezrc_allocator_free (test_allocator, string);

    check_test_allocator (test_allocator);
}

TheoryDataPoints (std_heap_wrapper, alloc_realloc_free) = {
    DataPoints(size_t, 2, 64, 1024, BUF_MAX),
    DataPoints(size_t, 2, 64, 1024, BUF_MAX),
};
Theory((size_t first_size, size_t second_size), std_heap_wrapper, alloc_realloc_free)
{
    cr_assume (first_size > 1);
    cr_assume (first_size <= BUF_MAX);
    cr_assume (second_size > 1);
    cr_assume (second_size <= BUF_MAX);

    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "std_heap_wrapper_alloc_realloc_free");

    char test_string[BUF_MAX];

    for (size_t i = 0; i < EZRC_MAX(first_size, second_size)-1; i++) test_string[i] = (char)((i + 48) % 126);
    test_string[EZRC_MAX(first_size, second_size)-1] = '\0';

    char *string = ezrc_allocator_alloc (test_allocator, first_size);
    cr_assert(ne(ptr, string, NULL));
    for (size_t i = 0; i < first_size-1; i++) string[i] = (char)((i + 48) % 126);
    string[first_size-1] = '\0';

    if (first_size <= second_size) {
        char old_char = test_string[first_size-1];
        test_string[first_size-1] = '\0';
        cr_assert(eq (str, string, test_string));
        test_string[first_size-1] = old_char;
    } else {
        cr_assert(eq (str, string, test_string));
    }

    char *new_string = ezrc_allocator_realloc (test_allocator, string, second_size);
    cr_assert (ne (ptr, new_string, NULL));
    for (size_t i = first_size-1; i < second_size-1; i++) new_string[i] = (char)((i + 48) % 126);
    new_string[second_size-1] = '\0';
    test_string[second_size-1] = '\0';
    cr_assert (eq (str, new_string, test_string));

    ezrc_allocator_free (test_allocator, new_string);

    check_test_allocator (test_allocator);
}

Test(std_heap_wrapper, empty_allocator)
{
    struct ezrc_allocator empty_allocator = {
        .alloc        = ezrc_std_heap_malloc_wrapper,
        .realloc      = NULL,
        .free         = NULL,
        .data         = NULL,
    };

    void *data = ezrc_allocator_alloc (empty_allocator, 16);
    cr_assert (ne (ptr, data, NULL));
    data = ezrc_allocator_alloc (empty_allocator, 16);
    cr_assert (ne (ptr, data, NULL));
    data = ezrc_allocator_realloc (empty_allocator, data, 32);
    cr_assert (eq (ptr, data, NULL));
    ezrc_allocator_free (empty_allocator, data);
    ezrc_allocator_free (empty_allocator, data);
}
