#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <ezrc/memory/allocator.h>
#include <ezrc/string_util.h>
#include <ezrc/error_codes.h>

#include "error_help.h"
#include "leak_check_helpers.h"

Test (string_util, format_string)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "string_util_format_string");
    struct ezrc_return return_val = EZRC_RETURN(EZRC_OK);

    char *format_success = NULL;
    return_val = ezrc_create_format_string (test_allocator, &format_success, "check%d", 2); /* 6 chars 1 '\0' -> size 7 */
    check_ezrc_return (return_val);
    cr_assert (ne (ptr, format_success, NULL));
    cr_assert (eq (str, format_success, "check2"));

    check_test_allocator (test_allocator);
}

Test (string_util, strdup)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "string_util_strdup");
    struct ezrc_return return_val = EZRC_RETURN(EZRC_OK);

    char *dup_success = ezrc_strdup_static (test_allocator, "abcde", &return_val); /* 6 chars allocated -> size left = 4 */
    check_ezrc_return (return_val);
    cr_assert (ne (ptr, dup_success, NULL));
    cr_assert (eq (str, dup_success, "abcde"));

    check_test_allocator (test_allocator);
}
