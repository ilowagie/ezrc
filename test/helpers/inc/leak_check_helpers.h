#ifndef LEAK_CHECK_HELPERS_H
#define LEAK_CHECK_HELPERS_H

#include <ezrc/base_structs.h>

struct ezrc_allocator init_test_allocator (struct ezrc_allocator base_allocator, char *test_name);
void check_test_allocator (struct ezrc_allocator test_allocator);

#endif
