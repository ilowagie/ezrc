#ifndef EZRC_FILE_HELPER_H
#define EZRC_FILE_HELPER_H

#include <stdio.h>
#include <unistd.h>

#include <ezrc/base_structs.h>
#include <ezrc/string_util.h>
#include <ezrc/memory/allocator.h>

static char *
get_test_file_name (struct ezrc_allocator allocator, char *base_name)
{
    static int iteration = 0;
    int pid = (int)getgid();
    char *name = NULL;
    ezrc_create_format_string (allocator, &name, "%d_i%d_%s", pid, iteration, base_name);
    return name;
}

#endif
