#ifndef EZRC_FAKERS_H
#define EZRC_FAKERS_H

#include <stdlib.h>

#include <ezrc/base_structs.h>

struct fake_struct_intern
{
    int   valid;
    char *test_string;
};
struct fake_struct
{
    size_t id;
    struct fake_struct_intern *intern;
};

struct fake_struct *make_fake_struct (struct ezrc_allocator allocator, size_t id);
void free_fake_struct (struct ezrc_allocator allocator, void *ptr);

#endif
