#ifndef ERROR_HELP_H
#define ERROR_HELP_H

#include <ezrc/error_codes.h>

void check_ezrc_return (struct ezrc_return info);
void check_ezrc_error_return (struct ezrc_return info, struct ezrc_return expected_return);

#endif
