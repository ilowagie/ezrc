#include <stdlib.h>
#include <unistd.h>

#include "leak_check_helpers.h"

#include <ezrc/memory/allocator.h>
#include <ezrc/string_util.h>

#include <criterion/criterion.h>
#include <criterion/new/assert.h>

struct ezrc_allocator
init_test_allocator (struct ezrc_allocator base_allocator, char *test_name)
{
    /* placeholder */
    cr_log_info ("%s:%s - placeholder function", __FILE_NAME__, __FUNCTION__);
    return base_allocator;
}

void
check_test_allocator (struct ezrc_allocator test_allocator)
{
    /* placeholder */
    cr_log_info ("%s:%s - placeholder function", __FILE_NAME__, __FUNCTION__);
}
