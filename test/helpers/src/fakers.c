#include "fakers.h"

#include <ezrc/memory/allocator.h>

struct fake_struct *
make_fake_struct (struct ezrc_allocator allocator, size_t id)
{
    struct fake_struct *fake = ezrc_allocator_alloc (allocator, sizeof (struct fake_struct));
    if (fake)
    {
        fake->intern              = ezrc_allocator_alloc (allocator, sizeof (struct fake_struct_intern));
        fake->intern->test_string = "test";
        fake->intern->valid       = 1;
        fake->id                  = id;
    }
    return fake;
}

void
free_fake_struct (struct ezrc_allocator allocator, void *ptr)
{
    struct fake_struct *fake = ptr;
    ezrc_allocator_free (allocator, fake->intern);
    ezrc_allocator_free (allocator, fake);
}
