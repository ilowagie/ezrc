#include "error_help.h"

#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <errno.h>
#include <string.h>

void
check_ezrc_return (struct ezrc_return info)
{
    if (info.code != EZRC_OK && info.code != EZRC_ERR_FILE_OP)
    {
        cr_fail ("[%s:%d] Got a non-zero return code: %d: %s",
                 info.file, info.line, info.code, ezrc_err_str (info.code));
    }
    else if (info.code != EZRC_OK)
    {
        cr_fail ("[%s:%d] Got a non-zero return code: %d: %s -- errno: %s",
                 info.file, info.line, info.code, ezrc_err_str (info.code), strerror (errno));
    }
}

void
check_ezrc_error_return (struct ezrc_return info, struct ezrc_return expected_return)
{
    if (info.code != expected_return.code)
    {
        cr_fail ("[%s:%d] Got code %d ([%s:%d] %s) while expecting code %d (%s)",
                 expected_return.file,
                 expected_return.line,
                 info.code,
                 info.file,
                 info.line,
                 ezrc_err_str (info.code),
                 expected_return.code,
                 ezrc_err_str (expected_return.code));
    }
}
