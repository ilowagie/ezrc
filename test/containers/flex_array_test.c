#include <stdlib.h>

#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <ezrc/memory/allocator.h>
#include <ezrc/containers/flex_array.h>

#include "fakers.h"
#include "leak_check_helpers.h"
#include "error_help.h"

/*  Test Bodies
 *  ===========
 */

void
test_int_array_inner (struct ezrc_allocator allocator, struct ezrc_flex_array *array)
{
    for (int i = 0; i < 66; i++)
    {
        ezrc_flex_array_append (int, array, i);
    }

    int elm_33 = ezrc_flex_array_get (int, array, 33);
    cr_assert (eq (int, elm_33, 33));
    int elm_2 = ezrc_flex_array_get (int, array, 2);
    cr_assert (eq (int, elm_2, 2));

    for (int i = 0; i < 66; i++)
    {
        int elm_i = ezrc_flex_array_get (int, array, i);
        cr_assert (eq (int, elm_i, i));
    }
    /* test the iterator */
    int *elm;
    size_t i = 0;
    ezrc_foreach (allocator, &(array->iterable), elm)
    {
        cr_assert (eq (int, *elm, i));
        if (ezrc_foreach_is_last) cr_assert (eq (int, *elm, 65));
        i++;
    }

    ezrc_flex_array_set (int, array, 77, 33);
    elm_33 = ezrc_flex_array_get (int, array, 33);
    cr_assert (eq (int, elm_33, 77));

    ezrc_flex_array_set (int, array, 77, 2);
    elm_2 = ezrc_flex_array_get (int, array, 2);
    cr_assert (eq (int, elm_2, 77));
}

struct flex_test_struct
{
    int valid;
    char *test_str;
    size_t id;
};

void
test_struct_array_inner (struct ezrc_allocator allocator, struct ezrc_flex_array *array)
{
    for (size_t i = 0; i < 20; i++)
    {
        ezrc_flex_array_append (struct flex_test_struct, array, ((struct flex_test_struct){
                .id = i,
                .test_str = "test test",
                .valid = 1,
        }));
    }

    struct flex_test_struct test_s3 = ezrc_flex_array_get (struct flex_test_struct, array, 3);
    cr_assert (eq (int, test_s3.valid, 1));
    cr_assert (eq (str, test_s3.test_str, "test test"));
    cr_assert (eq (sz, test_s3.id, 3));

    for (size_t i = 0; i < 20; i++)
    {
        struct flex_test_struct test_s = ezrc_flex_array_get (struct flex_test_struct, array, i);
        cr_assert (eq (int, test_s.valid, 1));
        cr_assert (eq (str, test_s.test_str, "test test"));
        cr_assert (eq (sz, test_s.id, i));
    }

    ezrc_flex_array_set (struct flex_test_struct, array, ((struct flex_test_struct){
        .id = 123,
        .test_str = "ooooh, keer iets anders",
        .valid = 1,
    }), 3);
    test_s3 = ezrc_flex_array_get (struct flex_test_struct, array, 3);
    cr_assert (eq (int, test_s3.valid, 1));
    cr_assert (eq (str, test_s3.test_str, "ooooh, keer iets anders"));
    cr_assert (eq (sz, test_s3.id, 123));
}

void
test_char_array_inner (struct ezrc_allocator allocator, struct ezrc_flex_array *array)
{
    check_ezrc_return (ezrc_flex_string_concat (array, "test first string."));
    check_ezrc_return (ezrc_flex_string_concat (array, " test second string!"));
    cr_assert (eq (str, (char *)(array->array), "test first string. test second string!"));
}

/*  Struct wrappers
 *  ===============
 */

/*      Normal Version
 *      --------------
 */

void
test_int_array (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array array;
    check_ezrc_return (ezrc_init_flex_array (&array, allocator, 2, sizeof (int)));
    test_int_array_inner (allocator, &array);
    ezrc_cleanup_flex_array (&array);
}
void
test_struct_array (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array array;
    check_ezrc_return (ezrc_init_flex_array (&array, allocator, 2, sizeof (struct flex_test_struct)));
    test_struct_array_inner (allocator, &array);
    ezrc_cleanup_flex_array (&array);
}
void
test_char_array (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array array;
    check_ezrc_return (ezrc_init_flex_array (&array, allocator, 2, sizeof (char)));
    test_char_array_inner (allocator, &array);
    ezrc_cleanup_flex_array (&array);
}

#ifdef EZRC_HAS_PTHREAD

/*      Thread-safe Version
 *      -------------------
 */

void
pre_test_check_array_ts (struct ezrc_flex_array_ts *array)
{
    check_ezrc_return (ezrc_array_get_rd_lock (array));
    check_ezrc_return (ezrc_array_get_rd_lock (array));
    check_ezrc_return (ezrc_array_release_lock (array));
    check_ezrc_return (ezrc_array_release_lock (array));
    check_ezrc_return (ezrc_array_get_wr_lock (array));
    check_ezrc_return (ezrc_array_release_lock (array));
}

void
post_test_check_array_ts (struct ezrc_flex_array_ts *array)
{
    check_ezrc_return (ezrc_array_get_wr_lock (array));
    check_ezrc_return (ezrc_array_release_lock (array));
}

void
test_int_array_ts (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array_ts array;
    check_ezrc_return (ezrc_init_flex_array_ts (&array, allocator, 2, sizeof (int)));
    test_int_array_inner (allocator, (struct ezrc_flex_array *)&array);
    pre_test_check_array_ts (&array);
    ezrc_cleanup_flex_array ((struct ezrc_flex_array *)&array);
    post_test_check_array_ts (&array);
}
void
test_struct_array_ts (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array_ts array;
    check_ezrc_return (ezrc_init_flex_array_ts (&array, allocator, 2, sizeof (struct flex_test_struct)));
    pre_test_check_array_ts (&array);
    test_struct_array_inner (allocator, (struct ezrc_flex_array *)&array);
    post_test_check_array_ts (&array);
    ezrc_cleanup_flex_array ((struct ezrc_flex_array *)&array);
}
void
test_char_array_ts (struct ezrc_allocator allocator)
{
    struct ezrc_flex_array_ts array;
    check_ezrc_return (ezrc_init_flex_array_ts (&array, allocator, 2, sizeof (char)));
    pre_test_check_array_ts (&array);
    test_char_array_inner (allocator, (struct ezrc_flex_array *)&array);
    post_test_check_array_ts (&array);
    ezrc_cleanup_flex_array ((struct ezrc_flex_array *)&array);
}

#endif

/*  Actual Tests
 *  ============
 */

/*      Std Allocator Tests
 *      -------------------
 */

Test (flex_array_std_alloc, int_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_int_array (test_allocator);
    check_test_allocator (test_allocator);
}
Test (flex_array_std_alloc, struct_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_struct_array (test_allocator);
    check_test_allocator (test_allocator);
}
Test (flex_array_std_alloc, char_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_char_array (test_allocator);
    check_test_allocator (test_allocator);
}

#ifdef EZRC_HAS_PTHREAD

Test (flex_array_ts_std_alloc, char_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_char_array_ts (test_allocator);
    check_test_allocator (test_allocator);
}
Test (flex_array_ts_std_alloc, struct_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_struct_array_ts (test_allocator);
    check_test_allocator (test_allocator);
}
Test (flex_array_ts_std_alloc, int_array)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "flex_array_int_array");
    test_int_array_ts (test_allocator);
    check_test_allocator (test_allocator);
}

#endif

/*      Arena Allocator Tests
 *      ---------------------
 */

#include <ezrc/memory/arena.h>

static const struct ezrc_arena_cfg arena_cfg = EZRC_DEFAULT_ARENA_CFG;

Test (flex_array_arena, int_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_int_array (test_allocator);
}
Test (flex_array_arena, struct_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_struct_array (test_allocator);
}
Test (flex_array_arena, char_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_char_array (test_allocator);
}

#ifdef EZRC_HAS_PTHREAD

Test (flex_array_ts_arena, char_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_char_array_ts (test_allocator);
}
Test (flex_array_ts_arena, struct_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_struct_array_ts (test_allocator);
}
Test (flex_array_ts_arena, int_array)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_int_array_ts (test_allocator);
}


/*      Thread-Safe Arena Allocator Tests
 *      ---------------------------------
 */

void
pre_test_check_arena_ts (struct ezrc_arena_ts *arena)
{
    check_ezrc_return (ezrc_arena_get_rd_lock (arena));
    check_ezrc_return (ezrc_arena_get_rd_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
    check_ezrc_return (ezrc_arena_get_wr_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
}

void
post_test_check_arena_ts (struct ezrc_arena_ts *arena)
{
    check_ezrc_return (ezrc_arena_get_wr_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
}

Test (flex_array_arena_ts, int_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_int_array (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}
Test (flex_array_ts_arena_ts, int_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_int_array_ts (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}

Test (flex_array_arena_ts, struct_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_struct_array (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}
Test (flex_array_ts_arena_ts, struct_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_struct_array_ts (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}

Test (flex_array_arena_ts, char_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_char_array (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}
Test (flex_array_ts_arena_ts, char_array)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_char_array_ts (test_allocator);
    post_test_check_arena_ts (&arena_ctx);
}

#endif
