#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <ezrc/base_structs.h>
#include <ezrc/macro_util.h>
#include <ezrc/containers/iterator.h>
#include <ezrc/memory/allocator.h>
#include <ezrc/containers/flex_array.h>

#include "leak_check_helpers.h"
#include "fakers.h"

struct test_struct
{
    struct ezrc_iterable iterable;
    size_t nrof_elms;
    int *array;
};

struct test_struct_iterator_ctx
{
    size_t cur_idx;
};

void *test_struct_next (struct ezrc_iterator iterator)
{
    struct test_struct *test_ref = ezrc_struct_of (iterator.iterable, struct test_struct, iterable);
    struct test_struct_iterator_ctx *iterator_ctx = iterator.ctx;
    if (iterator_ctx->cur_idx < test_ref->nrof_elms)
        return (void *)&test_ref->array[iterator_ctx->cur_idx++];
    else
        return NULL;
}

void *init_test_struct_iterator_ctx (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)
{
    struct test_struct_iterator_ctx *ctx = ezrc_allocator_alloc (allocator, sizeof (struct test_struct_iterator_ctx));
    if (ctx)
        ctx->cur_idx = 0;
    return ctx;
}

void free_test_struct_iterator_ctx (struct ezrc_allocator allocator, void *iterator_ctx)
{
    if (iterator_ctx)
    {
        struct test_struct_iterator_ctx *ctx = iterator_ctx;
        ezrc_allocator_free (allocator, ctx);
    }
}

Test (iterator, simple_iteration_test)
{
    size_t nrof_elms = 66;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "iterator_simple_iteration_test");

    struct test_struct test;
    test.iterable = (struct ezrc_iterable) {
        .next = test_struct_next,
        .init_ctx = init_test_struct_iterator_ctx,
        .free_ctx = free_test_struct_iterator_ctx,
    };
    test.nrof_elms = nrof_elms;
    test.array = ezrc_allocator_alloc (test_allocator, sizeof (int) * nrof_elms);

    /* filling array with elements */
    for (size_t i = 0; i < nrof_elms; i++)
    {
        test.array[i] = (int)i;
    }

    /* get iterator and check iteration */
    struct ezrc_iterator iterator = ezrc_iterator_get (test_allocator, &test.iterable);
    int *cur_elm = NULL;
    int i = 0;
    while ((cur_elm = ezrc_iterator_next (iterator)))
    {
        cr_assert (eq (int, *cur_elm, i));
        i++;
    }
    /* iterator should've cleaned itself up when reaching the end */

    /* only thing left freeing is the array we allocated */
    ezrc_allocator_free (test_allocator, test.array);

    check_test_allocator (test_allocator);
}

void *test_map_function (void *input, void *extern_args)
{
    struct fake_struct *fake_struct = *(struct fake_struct **)input;
    size_t *extern_arg = extern_args;
    fake_struct->id += (*extern_arg);
    return fake_struct;
}

Test (iterator, map_test)
{
    const size_t nrof_elms = 33;
    size_t id_addition = 2;
    struct ezrc_flex_array array;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "iterator_map_test");
    ezrc_init_flex_array (&array, test_allocator, 2, sizeof (struct fake_struct *));

    for (size_t i = 0; i < nrof_elms; i++)
    {
        struct fake_struct *elm = make_fake_struct (test_allocator, i);
        ezrc_flex_array_append (struct fake_struct *, &array, elm);
    }
    struct ezrc_iterator array_it = ezrc_iterator_get (test_allocator, &array.iterable);
    struct ezrc_iterator map_it = ezrc_iterator_map (array_it, test_map_function, &id_addition);
    struct fake_struct *cur_fake = NULL;
    size_t idx = 0;
    size_t expected_idx = 1;
    while ((cur_fake = ezrc_iterator_next (map_it)))
    {
        expected_idx = idx + id_addition;
        cr_assert (eq (int, cur_fake->intern->valid, 1));
        cr_assert (eq (sz, cur_fake->id, expected_idx));
        if (ezrc_iterator_is_at_end (map_it))
        {
            cr_assert (eq (sz, cur_fake->id, nrof_elms + id_addition - 1));
        }
        idx++;
    }
    struct fake_struct **cur_fake_d;
    ezrc_foreach (test_allocator, &array.iterable, cur_fake_d)
    {
        free_fake_struct (test_allocator, *cur_fake_d);
    }
    ezrc_cleanup_flex_array (&array);

    check_test_allocator (test_allocator);
}

int test_filter_function (void *input, void *extern_args)
{
    size_t input_elm = *(size_t *)input;
    size_t filter_cmp = *(size_t *)extern_args;
    if (input_elm < filter_cmp)
        return 1;
    return 0;
}

Test (iterator, filter_test)
{
    const size_t nrof_elms = 35;
    size_t filter_cmp = 10;
    struct ezrc_flex_array array;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "iterator_filter_test");
    ezrc_init_flex_array (&array, test_allocator, 2, sizeof (size_t));

    size_t nrof_expected_filter_elms = 0;
    for (size_t i = 0; i < nrof_elms; i++)
    {
        size_t insert_elm = i % 12;
        ezrc_flex_array_append (size_t, &array, insert_elm);
        if (insert_elm < filter_cmp) nrof_expected_filter_elms++;
    }

    struct ezrc_iterator array_it = ezrc_iterator_get (test_allocator, &array.iterable);
    struct ezrc_iterator filter_it = ezrc_iterator_filter (array_it, test_filter_function, &filter_cmp);
    size_t *cur_elm = NULL;

    size_t nrof_filter_elms = 0;
    while ((cur_elm = ezrc_iterator_next (filter_it)))
    {
        cr_assert (eq (sz, *cur_elm, (nrof_filter_elms % 10)));
        nrof_filter_elms++;
    }
    cr_assert (eq (sz, nrof_filter_elms, nrof_expected_filter_elms));

    ezrc_cleanup_flex_array (&array);
    check_test_allocator (test_allocator);
}

void test_fold_function (void *accumulator, void *elm, void *extern_args)
{
    int *i   = elm;
    int *sum = accumulator;

    *sum = (*sum) + (*i);
}

Test (iterator, fold_test)
{
    const size_t nrof_elms = 34;
    struct ezrc_flex_array array;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "iterator_fold_test");
    ezrc_init_flex_array (&array, test_allocator, 2, sizeof (int));

    for (size_t i = 0; i < nrof_elms; i++)
    {
        ezrc_flex_array_append (int, &array, 1);
    }
    struct ezrc_iterator array_it = ezrc_iterator_get (test_allocator, &array.iterable);
    int sum = 0;
    ezrc_iterator_fold (array_it, &sum, test_fold_function, NULL);

    cr_assert (eq (int, sum, nrof_elms));

    ezrc_cleanup_flex_array (&array);
    check_test_allocator (test_allocator);
}
