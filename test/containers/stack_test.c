#include <stdlib.h>
#include <limits.h>

#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include <criterion/theories.h>

#include <ezrc/macro_util.h>
#include <ezrc/containers/stack.h>
#include <ezrc/containers/iterator.h>
#include <ezrc/memory/allocator.h>

#include "fakers.h"
#include "leak_check_helpers.h"
#include "error_help.h"

/*  Test Bodies
 *  ===========
 */

void
test_basic_use_inner (struct ezrc_allocator allocator, struct ezrc_stack *stack, size_t nrof_elms)
{
    /* test alloc */
    /* test push */
    for (size_t i = 0; i < nrof_elms; i++)
    {
        struct fake_struct *fake = make_fake_struct (allocator, i);
        cr_assert (ne (ptr, fake, NULL));
        check_ezrc_return(ezrc_stack_push (stack, fake));
    }
    cr_assert (eq (sz, stack->len, nrof_elms));
    /* test foreach macros */
    struct fake_struct *fake;
    ezrc_foreach (allocator, &stack->iterable, fake)
    {
        cr_assert (ne (ptr, fake, NULL));
        cr_assert (eq (int, fake->intern->valid, 1));
        cr_assert (eq (str, fake->intern->test_string, "test"));
        if (ezrc_foreach_is_first)
            cr_assert (eq (sz, fake->id, 0));
        if (ezrc_foreach_is_last)
            cr_assert (eq (sz, fake->id, nrof_elms-1));
    }
    /* test pop */
    for (size_t i = nrof_elms; i > nrof_elms / 2; i--)
    {
        struct fake_struct *fake = ezrc_stack_pop (stack);
        cr_assert (ne (ptr, fake, NULL));
        cr_assert (eq (sz, fake->id, i-1));
        cr_assert (eq (int, fake->intern->valid, 1));
        cr_assert (eq (str, fake->intern->test_string, "test"));
        free_fake_struct (allocator, fake);
    }
    cr_assert (eq (sz, stack->len, nrof_elms / 2));
    /* test clear */
    ezrc_stack_clear (stack);
    cr_assert (eq (sz, stack->len, 0));
    /* test refill */
    for (size_t i = 0; i < nrof_elms / 2; i++)
    {
        struct fake_struct *fake = make_fake_struct (allocator, i);
        ezrc_stack_push (stack, fake);
    }
    cr_assert (eq (sz, stack->len, nrof_elms / 2));
    ezrc_foreach (allocator, &stack->iterable, fake)
    {
        cr_assert (ne (ptr, fake, NULL));
        cr_assert (eq (int, fake->intern->valid, 1));
        cr_assert (eq (str, fake->intern->test_string, "test"));
        if (ezrc_foreach_is_first)
            cr_assert (eq (sz, fake->id, 0));
        if (ezrc_foreach_is_last)
            cr_assert (eq (sz, fake->id, (nrof_elms / 2)-1));
    }
    /* free */
    ezrc_cleanup_stack (stack);
}

/*  Struct Wrappers
 *  ===============
 */

/*      Normal Version
 *      --------------
 */

void
test_basic_use (struct ezrc_allocator allocator, size_t nrof_elms)
{
    struct ezrc_stack stack;
    check_ezrc_return (ezrc_init_stack (&stack, 2, allocator, free_fake_struct));
    test_basic_use_inner (allocator, &stack, nrof_elms);
}

/*      Thread-Safe Version
 *      -------------------
 */

void
test_basic_use_ts (struct ezrc_allocator allocator, size_t nrof_elms)
{
    struct ezrc_stack_ts stack;
    check_ezrc_return (ezrc_init_stack_ts (&stack, 2, allocator, free_fake_struct));
    check_ezrc_return (ezrc_stack_get_rd_lock (&stack));
    check_ezrc_return (ezrc_stack_get_rd_lock (&stack));
    check_ezrc_return (ezrc_stack_release_lock (&stack));
    check_ezrc_return (ezrc_stack_release_lock (&stack));
    check_ezrc_return (ezrc_stack_get_wr_lock (&stack));
    check_ezrc_return (ezrc_stack_release_lock (&stack));
    test_basic_use_inner (allocator, (struct ezrc_stack *)&stack, nrof_elms);
    check_ezrc_return (ezrc_stack_get_wr_lock (&stack));
    check_ezrc_return (ezrc_stack_release_lock (&stack));
}

/*  Actual Tests
 *  ============
 */

/*      Std Allocator Tests
 *      -------------------
 */

TheoryDataPoints (stack_std_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_std_alloc, default_allocator_basic_use)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "stack_deafult_allocator_basic_use");
    test_basic_use (test_allocator, nrof_elms);
    check_test_allocator (test_allocator);
}

TheoryDataPoints (stack_ts_std_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_ts_std_alloc, default_allocator_basic_use)
{
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "stack_deafult_allocator_basic_use");
    test_basic_use_ts (test_allocator, nrof_elms);
    check_test_allocator (test_allocator);
}

/*      Arena Allocator Tests
 *      ---------------------
 */

#include <ezrc/memory/arena.h>

static const struct ezrc_arena_cfg arena_cfg = EZRC_DEFAULT_ARENA_CFG;

TheoryDataPoints (stack_arena_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_arena_alloc, default_allocator_basic_use)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_basic_use (test_allocator, nrof_elms);
}

TheoryDataPoints (stack_ts_arena_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_ts_arena_alloc, default_allocator_basic_use)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_basic_use_ts (test_allocator, nrof_elms);
}

/*      Thread-Safe Arena Allocator Tests
 *      ---------------------------------
 */

void
pre_test_check_arena_ts (struct ezrc_arena_ts *arena)
{
    check_ezrc_return (ezrc_arena_get_rd_lock (arena));
    check_ezrc_return (ezrc_arena_get_rd_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
    check_ezrc_return (ezrc_arena_get_wr_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
}

void
post_test_check_arena_ts (struct ezrc_arena_ts *arena)
{
    check_ezrc_return (ezrc_arena_get_wr_lock (arena));
    check_ezrc_return (ezrc_arena_release_lock (arena));
}

TheoryDataPoints (stack_arena_ts_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_arena_ts_alloc, default_allocator_basic_use)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    pre_test_check_arena_ts (&arena_ctx);
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    post_test_check_arena_ts (&arena_ctx);
    test_basic_use (test_allocator, nrof_elms);
}

TheoryDataPoints (stack_ts_arena_ts_alloc, default_allocator_basic_use) = {
    DataPoints (size_t, 2, 16, 64, 666, 4096),
};
Theory ((size_t nrof_elms), stack_ts_arena_ts_alloc, default_allocator_basic_use)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    pre_test_check_arena_ts (&arena_ctx);
    test_basic_use_ts (test_allocator, nrof_elms);
    post_test_check_arena_ts (&arena_ctx);
}
