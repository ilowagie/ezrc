#include <stdlib.h>

#include <criterion/criterion.h>
#include <criterion/new/assert.h>
#include <criterion/theories.h>

#include <ezrc/memory/allocator.h>
#include <ezrc/containers/hashmap.h>

#include "error_help.h"

/*  Test Bodies
 *  ===========
 */

void
test_string_int_map_inner (struct ezrc_hashmap *map, int store_key, int memcmp_after_col, int allow_overwrite)
{
    struct ezrc_return ret = EZRC_RETURN (EZRC_OK);

    cr_assert (eq (sz, map->nrof_rows, 4));

    ezrc_hashmap_put_rval (int, map, "zero", 0, ret);
    check_ezrc_return (ret);
    ezrc_hashmap_put_rval (int, map, "one", 1, ret);
    check_ezrc_return (ret);
    /* load factor checked on the next insert - load factor will be .75 */
    ezrc_hashmap_put_rval (int, map, "two", 2, ret);
    check_ezrc_return (ret);
    ezrc_hashmap_put_rval (int, map, "three", 3, ret);
    check_ezrc_return (ret);

    ezrc_hashmap_put_rval (int, map, "one", 1234, ret);
    if (allow_overwrite)
    {
        check_ezrc_return (ret);
    } else {
        check_ezrc_error_return (ret, EZRC_RETURN (EZRC_ERR_UNIQUE_VIOLATION));
        ret = EZRC_RETURN (EZRC_OK);
    }
    int *one_val = ezrc_hashmap_get (map, "one", &ret);
    check_ezrc_return (ret);
    if (allow_overwrite)
        cr_assert (eq (int, 1234, *one_val));
    else
        cr_assert (eq (int, 1, *one_val));

    ezrc_hashmap_put_rval (int, map, "four", 4, ret);
    check_ezrc_return (ret);
    /* load factor checked on next insert - load factor will be 1.5 */
    ezrc_hashmap_put_rval (int, map, "five", 5, ret);
    check_ezrc_return (ret);
    cr_assert (eq (sz, map->nrof_rows, 4));

    ezrc_hashmap_put_rval (int, map, "two", 321, ret);
    if (allow_overwrite)
    {
        check_ezrc_return (ret);
    } else {
        check_ezrc_error_return (ret, EZRC_RETURN (EZRC_ERR_UNIQUE_VIOLATION));
        ret = EZRC_RETURN (EZRC_OK);
    }

    int *two_val = ezrc_hashmap_get (map, "two", &ret);
    check_ezrc_return (ret);
    if (allow_overwrite)
        cr_assert (eq (int, 321, *two_val));
    else
        cr_assert (eq (int, 2, *two_val));

    int *five_val = ezrc_hashmap_get (map, "five", &ret);
    check_ezrc_return (ret);
    cr_assert (eq (int, 5, *five_val));

    ezrc_hashmap_put_rval (int, map, "six", 6, ret);
    check_ezrc_return (ret);

    /* when inserting seven the load factor will be 2.0 */
    ezrc_hashmap_put_rval (int, map, "seven", 7, ret);
    check_ezrc_return (ret);
    cr_assert (eq (sz, map->nrof_rows, 4));

    /* load factor checked on next insert - load factor will be 2.25 -> resize */
    ezrc_hashmap_put_rval (int, map, "eight", 8, ret);
    check_ezrc_return (ret);
    cr_assert (ne (sz, map->nrof_rows, 4));

    ezrc_hashmap_put_rval (int, map, "nine", 9, ret);
    check_ezrc_return (ret);

    int *six_val = ezrc_hashmap_get (map, "six", &ret);
    check_ezrc_return (ret);
    cr_assert (eq (int, 6, *six_val));

    int *nine_val = ezrc_hashmap_get (map, "nine", &ret);
    check_ezrc_return (ret);
    cr_assert (eq (int, 9, *nine_val));

    check_ezrc_return (ezrc_hashmap_del (map, "nine"));
    check_ezrc_return (ezrc_hashmap_del (map, "two"));
    check_ezrc_return (ezrc_hashmap_del (map, "seven"));

    six_val = ezrc_hashmap_get (map, "six", &ret);
    check_ezrc_return (ret);
    cr_assert (eq (int, 6, *six_val));

    two_val = ezrc_hashmap_get (map, "two", &ret);
    check_ezrc_error_return (ret, EZRC_RETURN (EZRC_ERR_NOT_FOUND));
    cr_assert (eq (ptr, two_val, NULL));

    check_ezrc_error_return (ezrc_hashmap_del (map, "nine"), EZRC_RETURN (EZRC_ERR_NOT_FOUND));
}

/*  Struct Wrappers
 *  ===============
 */

/*      Normal Version
 *      --------------
 */

void
test_string_int_map (struct ezrc_allocator allocator, int store_key, int memcmp_after_col, int allow_overwrite)
{
    struct ezrc_hashmap_cfg hashmap_cfg = EZRC_DEFAULT_HASHMAP_STR_KEY_CFG (sizeof (int));
    hashmap_cfg.store_key = store_key;
    hashmap_cfg.store_val = 1;
    hashmap_cfg.memcmp_after_col = memcmp_after_col;
    hashmap_cfg.initial_nrof_rows = 4;
    hashmap_cfg.size_check_intv = 2;
    hashmap_cfg.max_load_factor = 2.0;
    hashmap_cfg.min_load_factor = .25;
    hashmap_cfg.allow_overwrite = allow_overwrite;
    struct ezrc_hashmap map;

    check_ezrc_return (ezrc_hashmap_init (allocator, &map, hashmap_cfg));
    test_string_int_map_inner (&map, store_key, memcmp_after_col, allow_overwrite);

    check_ezrc_return (ezrc_hashmap_deinit (&map));
}

#ifdef EZRC_HAS_PTHREAD

/*      Thread-Safe Version
 *      -------------------
 */

void
pre_test_check_hashmap_ts (struct ezrc_hashmap_ts *map)
{
    check_ezrc_return (ezrc_hashmap_get_rd_lock (map));
    check_ezrc_return (ezrc_hashmap_get_rd_lock (map));
    check_ezrc_return (ezrc_hashmap_release_lock (map));
    check_ezrc_return (ezrc_hashmap_release_lock (map));
    check_ezrc_return (ezrc_hashmap_get_wr_lock (map));
    check_ezrc_return (ezrc_hashmap_release_lock (map));
}

void
post_test_check_hashmap_ts (struct ezrc_hashmap_ts *map)
{
    check_ezrc_return (ezrc_hashmap_get_wr_lock (map));
    check_ezrc_return (ezrc_hashmap_release_lock (map));
}

void
test_string_int_map_ts (struct ezrc_allocator allocator, int store_key, int memcmp_after_col, int allow_overwrite)
{
    struct ezrc_hashmap_cfg hashmap_cfg = EZRC_DEFAULT_HASHMAP_STR_KEY_CFG (sizeof (int));
    hashmap_cfg.store_key = store_key;
    hashmap_cfg.store_val = 1;
    hashmap_cfg.memcmp_after_col = memcmp_after_col;
    hashmap_cfg.initial_nrof_rows = 4;
    hashmap_cfg.size_check_intv = 2;
    hashmap_cfg.max_load_factor = 2.0;
    hashmap_cfg.min_load_factor = .25;
    hashmap_cfg.allow_overwrite = allow_overwrite;
    struct ezrc_hashmap_ts map;

    check_ezrc_return (ezrc_hashmap_init_ts (allocator, &map, hashmap_cfg));

    pre_test_check_hashmap_ts (&map);
    test_string_int_map_inner ((struct ezrc_hashmap *)&map, store_key, memcmp_after_col, allow_overwrite);

    post_test_check_hashmap_ts (&map);

    check_ezrc_return (ezrc_hashmap_deinit ((struct ezrc_hashmap *)&map));
}

#endif

/*  Actual Tests
 *  ============
 */

/*      Std Allocator Tests
 *      -------------------
 */

TheoryDataPoints (hashmap_std_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_std_alloc, string_int_map)
{
    struct ezrc_allocator test_allocator = EZRC_DEFAULT_ALLOCATOR;
    test_string_int_map (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

#ifdef EZRC_HAS_PTHREAD

TheoryDataPoints (hashmap_ts_std_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_ts_std_alloc, string_int_map)
{
    struct ezrc_allocator test_allocator = EZRC_DEFAULT_ALLOCATOR;
    test_string_int_map_ts (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

#endif

/*      Arena Allocator Tests
 *      ---------------------
 */

#include <ezrc/memory/arena.h>

static const struct ezrc_arena_cfg arena_cfg = EZRC_DEFAULT_ARENA_CFG;

TheoryDataPoints (hashmap_arena_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_arena_alloc, string_int_map)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_string_int_map (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

#ifdef EZRC_HAS_PTHREAD

TheoryDataPoints (hashmap_ts_arena_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_ts_arena_alloc, string_int_map)
{
    struct ezrc_arena arena_ctx;
    check_ezrc_return (ezrc_init_arena (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_string_int_map_ts (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

/*      Thread-Safe Arena Allocator Tests
 *      ---------------------------------
 */

TheoryDataPoints (hashmap_arena_ts_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_arena_ts_alloc, string_int_map)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_string_int_map (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

TheoryDataPoints (hashmap_ts_arena_ts_alloc, string_int_map) = {
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
    DataPoints (int, 1, 0),
};
Theory ((int store_key, int memcmp_after_col, int allow_overwrite), hashmap_ts_arena_ts_alloc, string_int_map)
{
    struct ezrc_arena_ts arena_ctx;
    check_ezrc_return (ezrc_init_arena_ts (&arena_ctx, arena_cfg));
    struct ezrc_allocator test_allocator = EZRC_ARENA_ALLOCATOR (&arena_ctx);
    test_string_int_map_ts (test_allocator, store_key, memcmp_after_col, allow_overwrite);
}

#endif
