#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include <ezrc/memory/allocator.h>
#include <ezrc/containers/linked_list.h>
#include <ezrc/macro_util.h>

#include "leak_check_helpers.h"
#include "error_help.h"

struct int_ll_node {
    struct ezrc_linked_list_node ll_node;
    int i;
};

void
linked_list_basics_test_inner (struct ezrc_allocator allocator, struct ezrc_linked_list_head *ll_head)
{
    for (int i = 0; i < 88; i++)
    {
        struct int_ll_node *node = ezrc_allocator_alloc (allocator, sizeof (struct int_ll_node));
        node->i = i;
        ezrc_linked_list_append_back (ll_head, &node->ll_node);
    }
    for (int i = 0; i < 88; i++)
    {
        struct ezrc_linked_list_node *node = ezrc_linked_list_pop_front (ll_head);
        cr_assert (ne (ptr, node, NULL));
        struct int_ll_node *int_node = ezrc_struct_of (node, struct int_ll_node, ll_node);
        cr_assert (eq (int, int_node->i, i));
        ezrc_allocator_free (allocator, int_node);
    }
    cr_assert (eq (ptr, ll_head->first, NULL));

    for (int i = 0; i < 88; i++)
    {
        struct int_ll_node *node = ezrc_allocator_alloc (allocator, sizeof (struct int_ll_node));
        node->i = i;
        ezrc_linked_list_append_front (ll_head, &node->ll_node);
    }
    for (int i = 0; i < 88; i++)
    {
        struct ezrc_linked_list_node *node = ezrc_linked_list_pop_back (ll_head);
        cr_assert (ne (ptr, node, NULL));
        struct int_ll_node *int_node = ezrc_struct_of (node, struct int_ll_node, ll_node);
        cr_assert (eq (int, int_node->i, i));
        ezrc_allocator_free (allocator, int_node);
    }
    cr_assert (eq (ptr, ll_head->first, NULL));
}

void
int_ll_fold_free_f (void *accumulator, void *elm, void *args)
{
    int *free_count = accumulator;
    struct ezrc_allocator *allocator = args;
    struct int_ll_node *node = ezrc_struct_of (elm, struct int_ll_node, ll_node);
    ezrc_allocator_free (*allocator, node);
    (*free_count)++;
}

void
linked_list_iterator_test_inner (struct ezrc_allocator allocator, struct ezrc_linked_list_head *ll_head)
{
    for (int i = 0; i < 88; i++)
    {
        struct int_ll_node *node = ezrc_allocator_alloc (allocator, sizeof (struct int_ll_node));
        node->i = i;
        ezrc_linked_list_append_back (ll_head, &node->ll_node);
    }
    struct ezrc_linked_list_node *cur_node;
    int i = 0;
    ezrc_foreach (allocator, &ll_head->iterable, cur_node)
    {
        struct int_ll_node *node = ezrc_struct_of (cur_node, struct int_ll_node, ll_node);
        cr_assert (eq (int, node->i, i));
        if (ezrc_foreach_is_last)
            cr_assert (eq (int, node->i, 87));
        else if (ezrc_foreach_is_first)
            cr_assert (eq (int, node->i, 0));
        i++;
    }
    int free_count = 0;
    struct ezrc_iterator ll_iter = ezrc_iterator_get (allocator, &ll_head->iterable);
    ezrc_iterator_fold (ll_iter, &free_count, int_ll_fold_free_f, &allocator);
    cr_assert (eq (int, free_count, 88));
}

Test (linked_list, basics)
{
    struct ezrc_linked_list_head ll_head;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "linked_list_basics");
    check_ezrc_return (ezrc_init_linked_list (&ll_head));

    linked_list_basics_test_inner (test_allocator, &ll_head);

    check_test_allocator (test_allocator);
}

void
pre_test_check_ll_ts (struct ezrc_linked_list_head_ts *head)
{
    check_ezrc_return (ezrc_linked_list_get_rd_lock (head));
    check_ezrc_return (ezrc_linked_list_get_rd_lock (head));
    check_ezrc_return (ezrc_linked_list_release_lock (head));
    check_ezrc_return (ezrc_linked_list_release_lock (head));
    check_ezrc_return (ezrc_linked_list_get_wr_lock (head));
    check_ezrc_return (ezrc_linked_list_release_lock (head));
}

void
post_test_check_ll_ts (struct ezrc_linked_list_head_ts *head)
{
    check_ezrc_return (ezrc_linked_list_get_wr_lock (head));
    check_ezrc_return (ezrc_linked_list_release_lock (head));
}

Test (linked_list_ts, basics)
{
    struct ezrc_linked_list_head_ts ll_head;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "linked_list_basics");
    check_ezrc_return (ezrc_init_linked_list_ts (&ll_head));

    pre_test_check_ll_ts (&ll_head);
    linked_list_basics_test_inner (test_allocator, (struct ezrc_linked_list_head *)&ll_head);
    post_test_check_ll_ts (&ll_head);

    check_test_allocator (test_allocator);
}

Test (linked_list, iterator)
{
    struct ezrc_linked_list_head ll_head;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "linked_list_basics");
    check_ezrc_return (ezrc_init_linked_list (&ll_head));

    linked_list_iterator_test_inner (test_allocator, &ll_head);

    check_test_allocator (test_allocator);
}

Test (linked_list_ts, iterator)
{
    struct ezrc_linked_list_head_ts ll_head;
    struct ezrc_allocator test_allocator = init_test_allocator (EZRC_DEFAULT_ALLOCATOR, "linked_list_basics");
    check_ezrc_return (ezrc_init_linked_list_ts (&ll_head));

    pre_test_check_ll_ts (&ll_head);
    linked_list_iterator_test_inner (test_allocator, (struct ezrc_linked_list_head *)&ll_head);
    post_test_check_ll_ts (&ll_head);

    check_test_allocator (test_allocator);
}
