Allocator
=========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/memory/allocator.h
~~~~~~~~~~~~~~~~~~~~~~~
Contains the base api for the ezrc_allocator

Functions
---------

ezrc_allocator_alloc
~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_allocator_alloc

ezrc_allocator_realloc
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_allocator_realloc

ezrc_allocator_free
~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_allocator_free

ezrc_std_heap_malloc_wrapper
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_std_heap_malloc_wrapper

ezrc_std_heap_realloc_wrapper
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_std_heap_realloc_wrapper

ezrc_std_heap_free_wrapper
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_std_heap_free_wrapper

Macros
------

EZRC_DEFAULT_ALLOCATOR
~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_DEFAULT_ALLOCATOR
