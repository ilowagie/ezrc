.. _iterator_ref:

Iterator
========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/containers/iterator.h
~~~~~~~~~~~~~~~~~~~~~~~~~~
Iterator API: a way to manipulate arrays

Structs
-------

ezrc_iterator
~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_iterator

ezrc_iterable
~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_iterable

ezrc_map_iterator_ctx
~~~~~~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_map_iterator_ctx

ezrc_filter_iterator_ctx
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_filter_iterator_ctx

Functions
---------

ezrc_iterator_get
~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_get

ezrc_iterator_next
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_next

ezrc_iterator_end
~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_end

ezrc_iterator_map
~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_map

ezrc_iterator_filter
~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_filter

ezrc_iterator_fold
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_iterator_fold

Macros
------

.. doxygendefine:: ezrc_foreach
.. doxygendefine:: ezrc_foreach_is_first
.. doxygendefine:: ezrc_foreach_is_last
.. doxygendefine:: ezrc_foreach_exit_loop
