.. _memory:

Memory
======

The Allocator
-------------

The memory module contains all things related to memory management.
The core struct used for memory management is the :c:struct:`ezrc_allocator` struct.
This struct is a representation for an allocator.
It can hold a context that is passed to the different allocation functions.

All functions in this library that need some sort of allocation will use this struct.
It will then use it's core functions:

- :c:func:`ezrc_allocator_alloc`
- :c:func:`ezrc_allocator_realloc`
- :c:func:`ezrc_allocator_free`

To use the heap allocation functions from the standard library, an :c:struct:`ezrc_allocator` struct containing wrappers around these functions is implemented in the :c:macro:`EZRC_DEFAULT_ALLOCATOR` macro.

The Arena Allocator
-------------------

Other then the default allocator, ezrc also provides its own arena allocator.
This arena allocator allows you to care less about freeing memory.
You can create an arena for a specific aspect of you're code, e.g. for everything related to a request that you're code is handling.
Then allocate everything with that arena, and once, for example, the request is handled, you can free all the allocations related to it with 1 single function.

To see the arena reference you can visit the :ref:`arena allocator reference<arena_ref>`
