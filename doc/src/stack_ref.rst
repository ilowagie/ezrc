.. _stack_ref:

Stack
=====

.. toctree::
   :maxdepth: 2

Header
------

ezrc/containers/stack.h
~~~~~~~~~~~~~~~~~~~~~~~
Contains everything for the ezrc_stack container

Structs
-------

ezrc_stack
~~~~~~~~~~
.. doxygenstruct:: ezrc_stack

Functions
---------

ezrc_init_stack
~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_stack

ezrc_stack_set_size
~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_set_size

ezrc_stack_push
~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_push

ezrc_stack_pop
~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_pop

ezrc_stack_clear
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_clear

ezrc_cleanup_stack
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_cleanup_stack

ezrc_init_stack_ts
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_stack_ts

ezrc_stack_get_rd_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_get_rd_lock

ezrc_stack_get_wr_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_get_wr_lock

ezrc_stack_release_lock
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_stack_release_lock
