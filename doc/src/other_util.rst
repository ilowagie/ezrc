Other Utilities
===============

Apart from allocators and containers, EZRC also contains some other utilities.

Macro Utilities
---------------

The macro utilities are kept in ``ezrc/macro_util.h``.
It includes some simple helpers for macros like :c:macro:`EZRC_STRINGIFY` and the :c:macro:`EZRC_MAX`,
as well as :c:macro:`ezrc_struct_of` for some pointer magic.

String Utilities
----------------

``ezrc/string_util.h`` contain some helpers for working with C strings.
See `string_util_ref` for the list of utilities.
