Containers
==========

.. _iterators:

Iterators
---------

An iterator is an abstraction to traverse a sequence of items.
This abstraction allows for chaining together actions to be done over a sequence without executing them immediately.

The iterator abstraction is held in the :c:struct:`ezrc_iterator` struct.
It can be obtained by calling :c:func:`ezrc_iterator_get`.
The iterator can then be constructed as long as an :c:struct:`ezrc_iterable` struct is defined.

The Iterable Struct
~~~~~~~~~~~~~~~~~~~

This :c:struct:`ezrc_iterable` struct basically contains the basic functions needed for iteration.
The most important function in this struct is the ``void *next (struct ezrc_iterator iterator)`` function, which takes an iterator and returns an element, or ``NULL`` if the end was reached.

This ``next`` function can make use of an iterator context, which will be constructed in the :c:func:`ezrc_iterator_get` function using the ``void *init_ctx (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)`` function from the :c:struct:`ezrc_iterable` struct.

When the iterator reaches the end, ``void free_ctx (struct ezrc_allocator allocator, void *iterator_ctx)`` from the :c:struct:`ezrc_iterable` struct will be called to free the created context immediately.

If you want to allow the iterator to check if the next element will be the last element, you can also add the ``int is_at_end (struct ezrc_iterator iterator)`` function to the iterable struct.
You can leave this one ``NULL`` as well without breaking the iterator.

The Iterator Struct
~~~~~~~~~~~~~~~~~~~

As mentioned above, an iterator can be obtained by calling :c:func:`ezrc_iterator_get`.

You can get elements from it using :c:func:`ezrc_iterator_next`.
This function will return ``NULL`` when reaching the end.
Once it does this, the iterator context (if there is any) will be freed.
The context can be freed earlier using :c:func:`ezrc_iterator_end` if you stop using the iterator before reaching the end.

Foreach
~~~~~~~

Around the next function, the :c:macro:`ezrc_foreach` was built for constructing loops over iterable structures.

Inside the foreach loop you can always add a check that's true for the first iteration in the loop: :c:macro:`ezrc_foreach_is_first`.
And if the ``is_at_end`` function is declared in the :c:func:`ezrc_iterable` struct, you can also add a check for the last iteration with :c:func:`ezrc_foreach_is_last`.

An early exit has to be done with :c:func:`ezrc_foreach_exit_loop` to clean up the iterator context.

Other Functionality Around the Iterator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Other than foreach, a function can be mapped on an iterator with :c:func:ezrc_iterator_map`.
The function will return a new iterator.
It only evaluates element per element when next is called.
If next is never called on the iterator, the function never evaluates.

One can also create a new iterator which filters out elements out of another iterator using the :c:func:`ezrc_iterator_filter` function.
Again, the filter will only be evaluated when the next function is called on the new iterator.

The :c:func:`ezrc_iterator_fold` function works a bit differently.
It will immediately iterate over all elements in the iterator.
It will fold the iterator into a single value.

.. note::
   All of the following containers are iterable.
   In each of them, the :c:struct:`ezrc_iterable` struct can be found in the ``iterable`` field which will be set by their ``init`` functions.

For more info see :ref:`the iterator reference documentation<iterator_ref>`

Flex Array
----------

The :c:struct:`ezrc_flex_array` container is a container that can hold an array with elements of a generic type.
The elements will all be stored next to each other in memory.
When appending elements, the array will grow if there's not enough space.
It will allocate more space then needed, so it doesn't have to reallocate the array in memory for every new element.

You can get elements from an arbitrary position in the array using :c:macro:`ezrc_flex_array_get`, or mutate them using :c:macro:`ezrc_flex_array_set`.

.. warning::
   To avoid undefined behavior or segmentation faults, it's important to keep the index within the array.
   There's no elegant error handling when trying access elements outside the flex array.

For more info see :ref:`the flex array reference documentation<flex_array_ref>`

Linked List
-----------

The linked list in EZRC is inspired by the linked list implementation in the Linux kernel.
Here, the nodes are defined by :c:struct:`ezrc_linked_list_node` structs, which can be placed inside the elements for the linked list.
The functions accessing the linked list will return pointers to this struct.
Using :c:macro:`ezrc_struct_of`, you can then obtain the elements themselves.

The list itself is represented with the :c:struct:`ezrc_linked_list_head`.
This contains a reference to the first element of the list as well as the :c:struct:`ezrc_iterable` struct that can be used to create an iterator for the list.

For more info see :ref:`the linked list reference documentation<linked_list_ref>`

Stack
-----

The :c:struct:`ezrc_stack` consists of a list of pointers.
Pointers can be pushed on or poped from the stack.

The stack can also take a ``free_elm`` funcion.
This will be called on each element when the stack itself is freed.

For more info see :ref:`the stack reference documentation<stack_ref>`
