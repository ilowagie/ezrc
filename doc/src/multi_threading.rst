Multi Threading
===============

If ``ezrc`` has been compiled with pthread, then it will support multithreading.
Every container and allocator will have a thread-safe version.

Thread safety here is implemented by means of a ``rwlock``, meaning that either multiple readers, or a single writer can take control of the lock at the same time.

Each container's thread-safe version of the struct will have a postfix ``_ts`` to it, which has to be initialized with the thread-safe version of the ``init`` function.

The pointers of these thread-safe structs can be casted to pointers of the non-thread-safe version to be safely passed on to the library's public API functions (not containing the ``__unsafe`` postfix).
Internaly, an int member, ``theadsafe``, will be checked to determine if a lock should be taken first or not, making those functions "safe".

You can always aquire and release the locks yourself with the ``_get_rd_lock``, ``get_wr_lock``, and ``release_lock`` functions.

.. warning::
   A thread can take the same lock multiple times, but it must release the lock as much as it has taken it.


Compiling with pthread
----------------------

To compile with pthread, the ``add_pthread`` from ``meson.options`` needs to be enabled.
It is enabled by default.

If this option is enabled, the ``EZRC_HAS_PTHREAD`` macro will be defined.
