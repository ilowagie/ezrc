.. _linked_list_ref:

Linked List
===========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/containers/linked_list.h
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contains the base api for the linked list container

Structs
-------

ezrc_linked_list_node
~~~~~~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_linked_list_node

ezrc_linked_list_head
~~~~~~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_linked_list_head

Functions
---------

ezrc_init_linked_list
~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_linked_list

ezrc_linked_list_append_back
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_append_back

ezrc_linked_list_append_front
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_append_front

ezrc_linked_list_pop_back
~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_pop_back

ezrc_linked_list_pop_front
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_pop_front

ezrc_init_linked_list_ts
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_linked_list_ts

ezrc_linked_list_get_rd_lock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_get_rd_lock

ezrc_linked_list_get_wr_lock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_get_wr_lock

ezrc_linked_list_release_lock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_linked_list_release_lock
