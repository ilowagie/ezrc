.. _string_util_ref:

String Util
===========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/string_util.h
~~~~~~~~~~~~~~~~~~
Contains some helpers for working with strings

Functions
---------

ezrc_create_format_string
~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_create_format_string

ezrc_strdup_static
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_strdup_static
