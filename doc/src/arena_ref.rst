.. _arena_ref:

Arena
=====

.. toctree::
   :maxdepth: 2

Header
------

ezrc/memory/arena.h
~~~~~~~~~~~~~~~~~~~
Contains the API of the ezrc arena allocator

Structs
-------

ezrc_arena_cfg
~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_arena_cfg

ezrc_arena_region
~~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_arena_region

ezrc_arena_block
~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_arena_block

ezrc_arena_page
~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_arena_page

ezrc_arena
~~~~~~~~~~
.. doxygenstruct:: ezrc_arena

Functions
---------

ezrc_init_arena
~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_arena

ezrc_arena_alloc
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_alloc

ezrc_arena_realloc
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_realloc

ezrc_arena_free
~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_free

ezrc_arena_clear
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_clear

ezrc_arena_cleanup
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_cleanup

ezrc_init_arena_ts
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_arena_ts

ezrc_arena_get_rd_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_get_rd_lock

ezrc_arena_get_wr_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_get_wr_lock

ezrc_arena_release_lock
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_arena_release_lock

Macros
------

EZRC_DEFAULT_ARENA_CFG
~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_DEFAULT_ARENA_CFG

EZRC_ARENA_ALLOCATOR
~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_ARENA_ALLOCATOR
