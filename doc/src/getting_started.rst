Getting Started
===============

Installation
------------

First, you need to setup the build directory with meson

.. code-block:: sh

   meson setup -Db_coverage=true build

The ``-Db_coverage=true`` is added to support coverage.
This command will create the ``build`` directory which will be used for all further meson commands.
If you already have a build directory, you can use the ``--reconfigure`` option to reconfigure it.

To compile the code, use

.. code-block:: sh

   meson compile -C build

To install everything to your system, use

.. code-block:: sh

   meson install -C build

Base Ideas and Concepts
-----------------------

Before using the library, you might want to take a look at some of the basic concepts that are used throughout the library.

Return Values and Errors
~~~~~~~~~~~~~~~~~~~~~~~~

All functions that can fail will either return, or take a pointer to, a :c:struct:`ezrc_return` struct.
This is a struct that contains a return code that can be converted to a string, as well as the line and file where the code was set.

If a function takes a pointer to this struct, you can set it to ``NULL`` if your not interested in it.
If it's not ``NULL`` it must be initialized to ``EZRC_RETURN(EZRC_OK)`` before calling the function to be useful.

Data Initialization
~~~~~~~~~~~~~~~~~~~

Most data structures in this library will have an initialization function that will not allocate the structure, but takes an allocated pointer, and initializes it.
This way, the stack can often be used for complex structs.

Allocators
~~~~~~~~~~

The allocation of data is abstracted away in this library.
Each function that would need to operate on the heap, will use an allocator instead of calling functions like ``malloc`` and ``free``.
The :c:struct:`ezrc_allocator` struct contains all the functions needed for memory management.

You can write your own allocator, or use one of the supplied allocators.
Most allocators in this library wrap around another one and provide additional functionality on top of the internal allocator.

See also the :ref:`memory section<memory>` of the documentation.

Iterators
~~~~~~~~~

This library provides the :c:struct:`ezrc_iterator` struct that can be used to iterate over iterable data.
Iterable data can be marked by implementing the :c:struct:`ezrc_iterable` struct.
This ``ezrc_iterable`` struct contains the functions needed for iterating.
Not all of these are required, some of them can be left as ``NULL``.

Iterators are useful for abstracting and lazy-evaluating operations over iterating data.
I.e. you can map a function over an iterator, which will return an iterator.
The mapping function will not evaluate on the data untill an element is obtained from the iterator.

See also the :ref:`iterator section<iterators>` of the documentation.
