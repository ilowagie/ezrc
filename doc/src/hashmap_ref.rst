.. _hashmap_ref:

Hashmap
=======

.. toctree::
   :maxdepth: 2

Header
------

ezrc/containers/hashmap.h
~~~~~~~~~~~~~~~~~~~~~~~~~
Contains the API for the ezrc hashmap container.

Structs
-------

ezrc_hashmap_cfg
~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_hashmap_cfg

ezrc_hashmap_elm
~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_hashmap_elm

ezrc_hashmap_row
~~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_hashmap_row

ezrc_hashmap
~~~~~~~~~~~~
.. doxygenstruct:: ezrc_hashmap

Functions
---------

ezrc_hashmap_str_len
~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_str_len

ezrc_hashmap_init
~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_init

ezrc_hashmap_put
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_put

ezrc_hashmap_get
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_get

ezrc_hashmap_del
~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_del

ezrc_hashmap_deinit
~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_deinit

ezrc_hashmap_init_ts
~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_init_ts

ezrc_hashmap_get_rd_lock
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_get_rd_lock

ezrc_hashmap_get_wr_lock
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_get_wr_lock

ezrc_hashmap_release_lock
~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_hashmap_release_lock


Macros
------

EZRC_HASHMAP_HASH_SEED
~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_HASHMAP_HASH_SEED

EZRC_DEFAULT_HASHMAP_CFG
~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_DEFAULT_HASHMAP_CFG

EZRC_DEFAULT_HASHMAP_STR_KEY_CFG
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: EZRC_DEFAULT_HASHMAP_STR_KEY_CFG

ezrc_hashmap_put_rval
~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: ezrc_hashmap_put_rval
