.. _flex_array_ref:

Flex Array
==========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/containers/flex_array.h
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contains the flex_array structure for creating dynamic arrays in C.

Structs
-------

ezrc_flex_array
~~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_flex_array

Functions
---------

ezrc_init_flex_array
~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_flex_array

ezrc_flex_array_room_for_one_more
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_flex_array_room_for_one_more

ezrc_flex_string_concat
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_flex_string_concat

ezrc_cleanup_flex_array
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_cleanup_flex_array

Macros
------

ezrc_flex_array_append
~~~~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: ezrc_flex_array_append

ezrc_flex_array_set
~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: ezrc_flex_array_set

ezrc_flex_array_get
~~~~~~~~~~~~~~~~~~~
.. doxygendefine:: ezrc_flex_array_get

ezrc_init_flex_array_ts
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_init_flex_array_ts

ezrc_array_get_rd_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_array_get_rd_lock

ezrc_array_get_wr_lock
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_array_get_wr_lock

ezrc_array_release_lock
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_array_release_lock

