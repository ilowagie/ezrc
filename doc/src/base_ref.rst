Base
====

.. toctree::
   :maxdepth: 2

Headers
-------

ezrc/base_structs.h
~~~~~~~~~~~~~~~~~~~
Contains all the basic structs needed to use the rest of the library

ezrc/macro_util.h
~~~~~~~~~~~~~~~~~
Contains some basic macros to make life easier

ezrc/version.h
~~~~~~~~~~~~~~
Contains the 3 version numbers and the version string macro

Structs
-------

ezrc_allocator
~~~~~~~~~~~~~~
.. doxygenstruct:: ezrc_allocator

Macros
------
.. doxygendefine:: EZRC_STRINGIFY_S0
.. doxygendefine:: EZRC_STRINGIFY
.. doxygendefine:: EZRC_MAX
.. doxygendefine:: EZRC_VERSION_MAJOR
.. doxygendefine:: EZRC_VERSION_MINOR
.. doxygendefine:: EZRC_VERSION_PATCH
.. doxygendefine:: EZRC_VERSION_STRING
.. doxygendefine:: ezrc_struct_of
