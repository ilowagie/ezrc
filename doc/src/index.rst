Welcome to the EZRC documentation
=================================

EZRC is a C library with some helpers in pure C to make writing C eazier and avoid rewriting the same basic functionality.

.. warning::
   The library is still a work in progress

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   memory
   containers
   multi_threading
   other_util
   reference
