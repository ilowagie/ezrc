Error Codes
===========

.. toctree::
   :maxdepth: 2

Header
------

ezrc/error_codes.h
~~~~~~~~~~~~~~~~~~
Contains all the necessary things for error handling in ezrc.

Constants
---------

ezrc_error_code_strings_c
~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenvariable:: ezrc_error_code_strings_c

Enums
-----

ezrc_error_code
~~~~~~~~~~~~~~~
.. doxygenenum:: ezrc_error_code

Structs
-------

ezrc_return
~~~~~~~~~~~
.. doxygenstruct:: ezrc_return

Functions
---------

ezrc_err_str
~~~~~~~~~~~~
.. doxygenfunction:: ezrc_err_str

ezrc_set_opt_error
~~~~~~~~~~~~~~~~~~
.. doxygenfunction:: ezrc_set_opt_error

Macros
~~~~~~
.. doxygendefine:: EZRC_RETURN
