EZRC (eazier C)
===============

[![pipeline status](https://gitlab.com/ilowagie/ezrc/badges/main/pipeline.svg)](https://gitlab.com/ilowagie/ezrc/-/pipelines)
[![test coverage](https://gitlab.com/ilowagie/ezrc/badges/main/coverage.svg?job=HTML)](https://ilowagie.gitlab.io/ezrc/cov_index.html)

Ezrc is a library created by me, ilowagie, to avoid reimplementing the same utility functions.
It's goal is to make C eazier.

Usage
=====

To Learn how to use this project, please read the [docs](https://ilowagie.gitlab.io/ezrc/doc_index.html).

Build Dependencies
==================

- C compiler
- [meson](https://mesonbuild.com/): build system
- [ninja](https://ninja-build.org/): meson's default generator (faster alternative to GNU make)
- [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/): tool for finding dependencies
- [xxHash](https://github.com/Cyan4973/xxHash): Fast hashing library

Building
--------

First, you need to setup the build directory with meson

```sh
meson setup -Db_coverage=true build
```

The `-Db_coverage=true` is added to support coverage.
This command will create the `build` directory which will be used for all further meson commands.
If you already have a build directory, you can use the `--reconfigure` option to reconfigure it.

To compile the code, use

```sh
meson compile -C build
```

To install everything to your system, use

```sh
meson install -C build
```

Test Dependencies
=================

- [criterion](https://github.com/Snaipe/Criterion): Unit test framework

Running Tests
-------------

Once the build directory is set up, you can run the tests with

```sh
meson test -C build
```

Coverage Dependencies
=====================

- [gcovr](https://gcovr.com/en/stable/): Coverage Utility

Run Code Coverage
-----------------

If the build environment is set up with coverage enabled, and the tests ran, then you can generate coverage reports using

```sh
ninja coverage -C build
```

Note that this time, we don't use the `meson compile` commmand.
This is because of a bug in newer meson versions, that makes it so that meson doesn't recognize the coverage targets, but ninja still does.
If you have an old meson version, you can still use `meson compile coverage -C build`.

The coverage can now (normally) be viewed in an HTML format.
Open `build/meson-logs/coveragereport/index.html` in a browser for a detailed view.

Doc Dependencies
================

- [python3](https://www.python.org): Should already be installed as a meson depenedency
- [sphinx](https://www.sphinx-doc.org/en/master/): For generating html out of rst files
- [doxygen](https://www.doxygen.nl/index.html): For extracting documentation from header files
- [breathe](https://breathe.readthedocs.io/en/latest/index.html): Allows integration of doxygen into sphinx
- [sphinx_rtd_theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html): Used theme for sphinx HTML

Building Documentation
----------------------

Once the build environment is setup (or reconfigured **after** installing sphinx and doxygen), you can use the following command to generate the documentation:

```sh
meson compile doc -C build
```

The documentation can now be browsed by opening `build/doc/index.html` in a browser.
