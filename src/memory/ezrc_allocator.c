#include "ezrc/base_structs.h"
#include "ezrc/memory/allocator.h"

void *
ezrc_allocator_alloc (struct ezrc_allocator allocator, const size_t size)
{
    return allocator.alloc (size, allocator.data);
}

void *
ezrc_allocator_realloc (struct ezrc_allocator allocator, void *ptr, const size_t new_size)
{
    if (allocator.realloc)
        return allocator.realloc (ptr, new_size, allocator.data);
    else
        return NULL;
}

void
ezrc_allocator_free (struct ezrc_allocator allocator, void *ptr)
{
    if (allocator.free) allocator.free (ptr, allocator.data);
}

/**
 *      STDLIB WRAPPERS
 *      ===============
 */

void *
ezrc_std_heap_malloc_wrapper (const size_t size, void *data)
{
    return malloc (size);
}

void *
ezrc_std_heap_realloc_wrapper (void *ptr, const size_t new_size, void *data)
{
    return realloc (ptr, new_size);
}

void
ezrc_std_heap_free_wrapper (void *ptr, void *data)
{
    free (ptr);
}
