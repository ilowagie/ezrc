#include <unistd.h>
#include <assert.h>
#include <sys/mman.h>
#include <string.h>

#include <ezrc/memory/arena.h>
#include <ezrc/macro_util.h>

void
reset_region (struct ezrc_arena_region *reg)
{
    reg->blk_used_size    = 0;
    reg->largest_free_blk = 0;
    reg->ptr_last         = reg->mem_data;
    reg->last_blk         = NULL;
    ezrc_init_linked_list (&reg->free_blks);
    ezrc_init_linked_list (&reg->used_blks);
}

struct ezrc_return
add_new_region (struct ezrc_arena *arena)
{
    /* set mmap config */
    size_t page_size = getpagesize ();
    int mmap_flags = MAP_ANONYMOUS | MAP_SHARED;
    /* get page */
    void *page = mmap (NULL, page_size, PROT_READ | PROT_WRITE, mmap_flags, -1, 0);
    if (page == (void *)-1)
    return EZRC_RETURN (EZRC_ERR_FILE_OP);
    /* init new page's metadata */
    struct ezrc_arena_region *reg = page;
    reg->capacity = page_size - offsetof (struct ezrc_arena_region, mem_data);
    reset_region (reg);
    ezrc_linked_list_append_back (&arena->regions, &reg->ll_region);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_init_arena (struct ezrc_arena *arena, struct ezrc_arena_cfg cfg)
{
    arena->cfg = cfg;
    arena->size_mask = ~((1 << cfg.size_gran) - 1);
    arena->threadsafe = 0;
    ezrc_init_linked_list (&arena->regions);
    ezrc_init_linked_list (&arena->pages);
    /* append new page to linked list of arena */
    return add_new_region (arena);
}

struct ezrc_arena_block *
get_blk_with_room (const size_t size, struct ezrc_arena_region *reg)
{
    if (reg->largest_free_blk >= size)
    {
        size_t largest_size_n = 0;
        size_t second_largest_size = 0;
        size_t smallest_size_diff = ~0;
        struct ezrc_arena_block *best_blk = NULL;
        struct ezrc_arena_block *blk = NULL;
        struct ezrc_arena_block *cur_blk = ezrc_struct_of (reg->free_blks.first, struct ezrc_arena_block, ll);
        int maintenance_needed = 1;
        if (cur_blk->ll.next == NULL)
        {
            /* we will take the one block left */
            maintenance_needed    = 0;
            reg->largest_free_blk = 0;
            /* no loop needed anymore */
            blk     = cur_blk;
            cur_blk = NULL;
        }
        while (cur_blk != NULL)
        {
            /* part 1 of loop: find the best block */
            if (blk == NULL)
            {
                if (cur_blk->size == size)
                {
                    blk = cur_blk;
                    if (blk->size != reg->largest_free_blk)
                    {
                        /* maintenance not needed anymore */
                        maintenance_needed = 0;
                        break;
                    }
                } else if (cur_blk->size > size) {
                    size_t size_diff = cur_blk->size - size;
                    if (size_diff < smallest_size_diff)
                    {
                        smallest_size_diff = size_diff;
                        best_blk = cur_blk;
                    }
                }
            }
            /* part 2: maintenance: find second largest free size in case we need the largest */
            if (cur_blk->size == reg->largest_free_blk) largest_size_n++;
            else if (cur_blk->size > second_largest_size) second_largest_size = cur_blk->size;
            cur_blk = ezrc_struct_of (cur_blk->ll.next, struct ezrc_arena_block, ll);
        }
        if (blk == NULL) blk = best_blk;
        if (maintenance_needed && blk->size == reg->largest_free_blk)
        {
            if (largest_size_n < 2) reg->largest_free_blk = second_largest_size;
        }
        assert (blk != NULL);
        blk->free = 0;
        blk->full_page = 0;
        struct ezrc_return ret = ezrc_linked_list_remove (&reg->free_blks, &blk->ll);
        assert (ret.code == EZRC_OK);
        ezrc_linked_list_append_back (&reg->used_blks, &blk->ll);
        return blk;
    }
    return NULL;
}

struct ezrc_arena_block *
grow_reg_get_blk (const size_t processed_sz, struct ezrc_arena_region *reg)
{
    if (reg->capacity >= processed_sz)
    {
        struct ezrc_arena_block *blk = reg->ptr_last;
        reg->ptr_last      += processed_sz;
        reg->capacity      -= processed_sz;
        reg->blk_used_size += processed_sz;
        blk->size = processed_sz - offsetof (struct ezrc_arena_block, mem_data);
        blk->free = 0;
        blk->full_page = 0;
        blk->reg = reg;
        blk->prev = reg->last_blk;
        reg->last_blk = blk;
        ezrc_linked_list_append_back (&reg->used_blks, &blk->ll);
        return blk;
    }
    return NULL;
}

void *
ezrc_arena_alloc__unsafe (const size_t size, void *data)
{
    assert (data != NULL);
    struct ezrc_arena *arena = data;
    /* first check if allocation needs its own page */
    if (getpagesize () - arena->cfg.large_mem_rest_thres <= ((size + ~arena->size_mask) & arena->size_mask))
    {
        size_t page_size = size + offsetof (struct ezrc_arena_block, mem_data) + offsetof (struct ezrc_arena_page, blk);
        size_t round_size = (size + ~arena->size_mask) & arena->size_mask;
        int mmap_flags = MAP_ANONYMOUS | MAP_SHARED;
        void *page = mmap (0, page_size, PROT_READ | PROT_WRITE, mmap_flags, -1, 0);
        assert (page != (void *)-1);
        struct ezrc_arena_page *arena_page = page;
        arena_page->blk.size = round_size;
        arena_page->blk.free = 0;
        arena_page->blk.full_page = 1;
        ezrc_linked_list_append_back (&arena->pages, &arena_page->ll_page);
        return arena_page->blk.mem_data;
    }
    /* if not, get a free spot */
    struct ezrc_arena_region *last_reg = NULL;
    struct ezrc_arena_region *reg = ezrc_struct_of (arena->regions.first, struct ezrc_arena_region, ll_region);
    size_t processed_sz = (size + offsetof (struct ezrc_arena_block, mem_data) + ~arena->size_mask) & arena->size_mask;
    struct ezrc_arena_block *blk;
    while (reg != NULL)
    {
        if ((blk = get_blk_with_room (size, reg))
         || (blk = grow_reg_get_blk (processed_sz, reg)))
            return (blk->mem_data);
        last_reg = reg;
        reg = ezrc_struct_of (reg->ll_region.next, struct ezrc_arena_region, ll_region);
    }
    /* if we haven't returned at this point, we might need a new region -- if allowed */
    if (!arena->cfg.static_size)
    {
        struct ezrc_return ret = add_new_region (arena);
        assert (ret.code == EZRC_OK);
        /* new page could either be completely new or merged into the last one */
        /* first check if it's a new one */
        reg = ezrc_struct_of (last_reg->ll_region.next, struct ezrc_arena_region, ll_region);
        if (reg)
            if ((blk = get_blk_with_room (size, reg))
             || (blk = grow_reg_get_blk (processed_sz, reg)))
                return (blk->mem_data);
    }
    return NULL;
}

void *
ezrc_arena_alloc (const size_t size, void *data)
{
    struct ezrc_arena *arena = (struct ezrc_arena *)data;
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        if (pthread_rwlock_wrlock (&arena_ts->rwlock)) return NULL;
    }
#endif
    void *ptr = ezrc_arena_alloc__unsafe (size, data);
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        pthread_rwlock_unlock (&arena_ts->rwlock);
    }
#endif
    return ptr;
}

void
ezrc_arena_free__unsafe (void *ptr, void *data)
{
    assert (ptr != NULL);
    assert (data != NULL);
    struct ezrc_arena_block *blk = ezrc_struct_of (ptr, struct ezrc_arena_block, mem_data);
    assert (blk->free == 0);
    struct ezrc_arena *arena = data;
    if (blk->full_page)
    {
        struct ezrc_arena_page *page = ezrc_struct_of (blk, struct ezrc_arena_page, blk);
        size_t page_size = page->blk.size + offsetof (struct ezrc_arena_page, blk) + offsetof (struct ezrc_arena_block, mem_data);
        ezrc_linked_list_remove (&arena->pages, &page->ll_page);
        munmap (page, page_size);
    } else {
        struct ezrc_return ret;
        ret = ezrc_linked_list_remove (&blk->reg->used_blks, &blk->ll);
        assert (ret.code == EZRC_OK);
        if (blk->prev && blk->prev->free)
        {
            /* if previous block is also free: merge blocks together */
            if (blk->reg->last_blk == blk) blk->reg->last_blk = blk->prev;
            blk->prev->size += (blk->size + offsetof (struct ezrc_arena_block, mem_data));
            if (blk->prev->size > blk->reg->largest_free_blk) blk->reg->largest_free_blk = blk->prev->size;
        } else {
            blk->free = 1;
            ezrc_linked_list_append_back (&blk->reg->free_blks, &blk->ll);
            if (blk->size > blk->reg->largest_free_blk) blk->reg->largest_free_blk = blk->size;
        }
    }
}

void *
ezrc_arena_realloc__unsafe (void *ptr, const size_t new_size, void *data)
{
    assert (ptr != NULL);
    assert (data != NULL);
    if (ptr == NULL) return ezrc_arena_alloc__unsafe (new_size, data);
    /* check if the used block is still large enought */
    struct ezrc_arena_block *blk = ptr - offsetof (struct ezrc_arena_block, mem_data);
    size_t old_size = blk->size;
    if (old_size >= new_size) return ptr;
    /* if not: alloc new data and free old if alloc was succesful */
    void *new_ptr = ezrc_arena_alloc__unsafe (new_size, data);
    if (new_ptr)
    {
        memcpy (new_ptr, ptr, old_size);
        ezrc_arena_free__unsafe (ptr, data);
    }
    return new_ptr;
}

void *
ezrc_arena_realloc (void *ptr, const size_t new_size, void *data)
{
    struct ezrc_arena *arena = (struct ezrc_arena *)data;
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        if (pthread_rwlock_wrlock (&arena_ts->rwlock)) return NULL;
    }
#endif
    void *new_ptr = ezrc_arena_realloc__unsafe (ptr, new_size, data);
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        pthread_rwlock_unlock (&arena_ts->rwlock);
    }
#endif
    return new_ptr;
}

void
ezrc_arena_free (void *ptr, void *data)
{
    struct ezrc_arena *arena = (struct ezrc_arena *)data;
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        if (pthread_rwlock_wrlock (&arena_ts->rwlock)) return;
    }
#endif
    ezrc_arena_free__unsafe (ptr, data);
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        pthread_rwlock_unlock (&arena_ts->rwlock);
    }
#endif
}

void
ezrc_arena_clear (struct ezrc_arena *arena)
{
    assert (arena != NULL);
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        if (pthread_rwlock_wrlock (&arena_ts->rwlock)) return;
    }
#endif
    struct ezrc_arena_page *cur_page = ezrc_struct_of (arena->pages.first, struct ezrc_arena_page, ll_page);
    while (cur_page)
    {
        size_t page_size = cur_page->blk.size + offsetof (struct ezrc_arena_page, blk) + offsetof (struct ezrc_arena_block, mem_data);
        struct ezrc_linked_list_node *next_node = cur_page->ll_page.next;
        munmap (cur_page, page_size);
        cur_page = ezrc_struct_of (next_node, struct ezrc_arena_page, ll_page);
    }
    struct ezrc_arena_region *first_reg = ezrc_struct_of (arena->regions.first, struct ezrc_arena_region, ll_region);
    assert (first_reg != NULL);
    first_reg->capacity += first_reg->blk_used_size;
    reset_region (first_reg);
    struct ezrc_arena_region *cur_reg = ezrc_struct_of (first_reg->ll_region.next, struct ezrc_arena_region, ll_region);
    first_reg->ll_region.next = NULL;
    while (cur_reg)
    {
        size_t page_size = cur_reg->capacity + cur_reg->blk_used_size + offsetof (struct ezrc_arena_region, mem_data);
        struct ezrc_linked_list_node *next_node = cur_reg->ll_region.next;
        munmap (cur_reg, page_size);
        cur_reg = ezrc_struct_of (next_node, struct ezrc_arena_region, ll_region);
    }
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        pthread_rwlock_unlock (&arena_ts->rwlock);
    }
#endif
}

void
ezrc_arena_cleanup (struct ezrc_arena *arena)
{
    assert (arena != NULL);
#ifdef EZRC_HAS_PTHREAD
    if (arena->threadsafe)
    {
        struct ezrc_arena_ts *arena_ts = (struct ezrc_arena_ts *)arena;
        if (pthread_rwlock_destroy (&arena_ts->rwlock)) return;
    }
#endif
    struct ezrc_arena_page *cur_page = ezrc_struct_of (arena->pages.first, struct ezrc_arena_page, ll_page);
    while (cur_page)
    {
        size_t page_size = cur_page->blk.size + offsetof (struct ezrc_arena_page, blk) + offsetof (struct ezrc_arena_block, mem_data);
        struct ezrc_linked_list_node *next_node = cur_page->ll_page.next;
        munmap (cur_page, page_size);
        cur_page = ezrc_struct_of (next_node, struct ezrc_arena_page, ll_page);
    }
    struct ezrc_arena_region *cur_reg = ezrc_struct_of (arena->regions.first, struct ezrc_arena_region, ll_region);
    while (cur_reg)
    {
        size_t page_size = cur_reg->capacity + cur_reg->blk_used_size + offsetof (struct ezrc_arena_region, mem_data);
        struct ezrc_linked_list_node *next_node = cur_reg->ll_region.next;
        munmap (cur_reg, page_size);
        cur_reg = ezrc_struct_of (next_node, struct ezrc_arena_region, ll_region);
    }
}

#ifdef EZRC_HAS_PTHREAD


struct ezrc_return
ezrc_init_arena_ts (struct ezrc_arena_ts *arena, struct ezrc_arena_cfg cfg)
{
    struct ezrc_return ret = ezrc_init_arena ((struct ezrc_arena *)arena, cfg);
    if (ret.code != EZRC_OK) return ret;
    arena->threadsafe = 1;
    if (pthread_rwlock_init (&arena->rwlock, NULL)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_arena_get_rd_lock (struct ezrc_arena_ts *arena)
{
    if (!arena) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!arena->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_rdlock (&arena->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_arena_get_wr_lock (struct ezrc_arena_ts *arena)
{
    if (!arena) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!arena->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_wrlock (&arena->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_arena_release_lock (struct ezrc_arena_ts *arena)
{
    if (!arena) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!arena->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    pthread_rwlock_unlock (&arena->rwlock);
    return EZRC_RETURN (EZRC_OK);
}

#endif
