#include "ezrc/containers/stack.h"

#include "ezrc/memory/allocator.h"
#include "ezrc/macro_util.h"

void *
ezrc_stack_iter_next (struct ezrc_iterator iterator)
{
    if ((!iterator.iterable) || (!iterator.ctx)) return NULL;
    struct ezrc_stack *stack   = ezrc_struct_of (iterator.iterable, struct ezrc_stack, iterable);
    size_t            *cur_idx = iterator.ctx;
    if ((*cur_idx) < stack->len)
        return stack->elms[(*cur_idx)++];
    else
        return NULL;
}

void *
ezrc_stack_iter_init_ctx (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)
{
    size_t *cur_idx = ezrc_allocator_alloc (allocator, sizeof (size_t));
    if (cur_idx)
        *cur_idx = 0;
    return cur_idx;
}

void
ezrc_stack_iter_free_ctx (struct ezrc_allocator allocator, void *iterator_ctx)
{
    ezrc_allocator_free (allocator, iterator_ctx);
}

int
ezrc_stack_iter_is_at_end (struct ezrc_iterator iterator)
{
    if ((!iterator.iterable) || (!iterator.ctx)) return 1;
    struct ezrc_stack *stack   = ezrc_struct_of (iterator.iterable, struct ezrc_stack, iterable);
    size_t            *cur_idx = iterator.ctx;
    if (stack) return (*cur_idx) == stack->len;
    return 0;
}

struct ezrc_return
ezrc_init_stack (struct ezrc_stack *stack,
                 const size_t init_cap,
                 struct ezrc_allocator allocator,
                 void (*free_elm) (struct ezrc_allocator, void *))
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);

#ifdef EZRC_HAS_PTHREAD
    stack->threadsafe = 0;
#endif

    size_t init_size = init_cap * sizeof (void *);
    stack->elms = ezrc_allocator_alloc (allocator, init_size);
    if (!stack->elms)
    {
        ezrc_allocator_free (allocator, stack);
        return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    }

    stack->cap       = init_cap;
    stack->len       = 0;
    stack->allocator = allocator;
    stack->free_elm  = free_elm;
    stack->iterable  = (struct ezrc_iterable){
        .next      = ezrc_stack_iter_next,
        .init_ctx  = ezrc_stack_iter_init_ctx,
        .free_ctx  = ezrc_stack_iter_free_ctx,
        .is_at_end = ezrc_stack_iter_is_at_end,
    };

    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_stack_set_size__unsafe (struct ezrc_stack *stack, const size_t size)
{
    if (size < stack->len) return EZRC_RETURN (EZRC_ERR_INTERNAL_CONTRADICTION);

    size_t new_size       = size;
    size_t new_alloc_size = size * sizeof (void *);
    void *new_ptr         = NULL;

    new_ptr = ezrc_allocator_realloc (stack->allocator, stack->elms, new_alloc_size);
    if (!new_ptr)
    {
        new_size = (stack->cap + stack->len) + 1;
        new_alloc_size = size * sizeof (void *);
        new_ptr  = ezrc_allocator_realloc (stack->allocator, stack->elms, new_alloc_size);
        if (!new_ptr) return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    }

    stack->cap  = new_size - stack->len;
    stack->elms = new_ptr;

    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_stack_set_size (struct ezrc_stack *stack, const size_t size)
{
#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        if (pthread_rwlock_wrlock (&stack_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif

    struct ezrc_return ret = ezrc_stack_set_size__unsafe (stack, size);

#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        pthread_rwlock_unlock (&stack_ts->rwlock);
    }
#endif

    return ret;
}

struct ezrc_return
ezrc_stack_push (struct ezrc_stack *stack, void *elm)
{
    if (!elm)   return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);

    struct ezrc_return info = EZRC_RETURN (EZRC_OK);

#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        if (pthread_rwlock_wrlock (&stack_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif

    if (stack->cap < 1)
    {
        size_t new_size = (stack->cap + stack->len) << 1;
        info = ezrc_stack_set_size__unsafe (stack, new_size);
        if (info.code != EZRC_OK) goto function_end;
    }

    stack->cap--;
    stack->elms[stack->len++] = elm;

function_end:
#ifdef EZRC_HAS_PTHREAD
        if (stack->threadsafe)
        {
            struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
            pthread_rwlock_unlock (&stack_ts->rwlock);
        }
#endif
    return info;
}

void *
ezrc_stack_pop (struct ezrc_stack *stack)
{
    if (!stack)          return NULL;
#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        if (pthread_rwlock_wrlock (&stack_ts->rwlock)) return NULL;
    }
#endif
    void *elm = NULL;
    if (stack->len == 0) goto function_end;
    elm = stack->elms[--stack->len];
    stack->cap++;
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        pthread_rwlock_unlock (&stack_ts->rwlock);
    }
#endif
    return elm;
}

struct ezrc_return
ezrc_stack_clear (struct ezrc_stack *stack)
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);

#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        if (pthread_rwlock_wrlock (&stack_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif

    if (stack->free_elm)
    {
        for (size_t i = 0; i < stack->len; i++) stack->free_elm (stack->allocator, stack->elms[i]);
    }
    stack->cap += stack->len;
    stack->len  = 0;

#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        pthread_rwlock_unlock (&stack_ts->rwlock);
    }
#endif
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_cleanup_stack (struct ezrc_stack *stack)
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    struct ezrc_return ret = ezrc_stack_clear (stack);
    if (ret.code != EZRC_OK) return ret;
#ifdef EZRC_HAS_PTHREAD
    if (stack->threadsafe)
    {
        struct ezrc_stack_ts *stack_ts = (struct ezrc_stack_ts *)stack;
        if (pthread_rwlock_destroy (&stack_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    ezrc_allocator_free (stack->allocator, stack->elms);
    return EZRC_RETURN (EZRC_OK);
}

#ifdef EZRC_HAS_PTHREAD

struct ezrc_return
ezrc_init_stack_ts (struct ezrc_stack_ts  *stack,
                    const size_t           init_cap,
                    struct ezrc_allocator  allocator,
                    void (*free_elm) (struct ezrc_allocator, void *))
{
    struct ezrc_return ret = ezrc_init_stack ((struct ezrc_stack *)stack, init_cap, allocator, free_elm);
    if (ret.code != EZRC_OK) return ret;
    stack->threadsafe = 1;
    if (pthread_rwlock_init (&stack->rwlock, NULL)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_stack_get_rd_lock (struct ezrc_stack_ts *stack)
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!stack->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_rdlock (&stack->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_stack_get_wr_lock (struct ezrc_stack_ts *stack)
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!stack->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_wrlock (&stack->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_stack_release_lock (struct ezrc_stack_ts *stack)
{
    if (!stack) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!stack->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    pthread_rwlock_unlock (&stack->rwlock);
    return EZRC_RETURN (EZRC_OK);
}

#endif
