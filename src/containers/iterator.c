#include "ezrc/containers/iterator.h"
#include "ezrc/memory/allocator.h"

struct ezrc_iterator
ezrc_iterator_get (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)
{
    void *ctx = iterable->init_ctx (allocator, iterable);
    int *is_new = ezrc_allocator_alloc (allocator, sizeof (int));
    if (is_new) *is_new = 2;
    return (struct ezrc_iterator) {
        .allocator = allocator,
        .iterable  = iterable,
        .ctx       = ctx,
        .is_new    = is_new,
    };
}

void *
ezrc_iterator_next (struct ezrc_iterator iterator)
{
    if ((!iterator.ctx) || (!iterator.iterable) || (!iterator.is_new)) return NULL;
    (*iterator.is_new) >>= 1;
    void *elm = NULL;
    if (iterator.iterable->next)
        elm = iterator.iterable->next (iterator);
    if (!elm) ezrc_iterator_end (iterator);
    return elm;
}

void
ezrc_iterator_end (struct ezrc_iterator iterator)
{
    ezrc_allocator_free (iterator.allocator, iterator.is_new);
    if (iterator.iterable && iterator.iterable->free_ctx)
        iterator.iterable->free_ctx (iterator.allocator, iterator.ctx);
}

int
ezrc_iterator_is_new (struct ezrc_iterator iterator)
{
    if (iterator.is_new) return *iterator.is_new;
    else return -1;
}

int
ezrc_iterator_is_at_end (struct ezrc_iterator iterator)
{
    if (iterator.iterable && iterator.iterable->is_at_end)
        return iterator.iterable->is_at_end (iterator);
    return 0;
}

static void *
ezrc_map_iterator_next (struct ezrc_iterator iterator)
{
    if (iterator.ctx)
    {
        struct ezrc_map_iterator_ctx *map_ctx = iterator.ctx;
        void *elm = ezrc_iterator_next (map_ctx->intern_iter);
        if (!elm)
        {
            return NULL;
        }
        return map_ctx->map_f (elm, map_ctx->extern_args);
    }
    return NULL;
}

static void
ezrc_map_iterator_free (struct ezrc_allocator allocator, void *ctx)
{
    if (ctx)
    {
        struct ezrc_map_iterator_ctx *map_ctx = ctx;
        ezrc_allocator_free (allocator, map_ctx);
    }
}

static int
ezrc_map_iterator_is_at_end (struct ezrc_iterator iterator)
{
    if (iterator.ctx)
    {
        struct ezrc_map_iterator_ctx *map_ctx = iterator.ctx;
        map_ctx->intern_iter.is_new = NULL;
        return ezrc_iterator_is_at_end (map_ctx->intern_iter);
    }
    return 0;
}

const struct ezrc_iterable map_iterable = {
    .next      = ezrc_map_iterator_next,
    .init_ctx  = NULL,
    .free_ctx  = ezrc_map_iterator_free,
    .is_at_end = ezrc_map_iterator_is_at_end,
};

struct ezrc_iterator
ezrc_iterator_map (struct ezrc_iterator iterator, void *(*map_f) (void *, void *), void *extern_args)
{
    struct ezrc_map_iterator_ctx *map_ctx = ezrc_allocator_alloc (iterator.allocator, sizeof (struct ezrc_map_iterator_ctx));
    if (map_ctx)
    {
        map_ctx->map_f       = map_f;
        map_ctx->intern_iter = iterator;
        map_ctx->extern_args = extern_args;
    }
    int *is_new = ezrc_allocator_alloc (iterator.allocator, sizeof (int));
    if (is_new) *is_new = 2;
    return (struct ezrc_iterator) {
        .allocator = iterator.allocator,
        .iterable  = &map_iterable,
        .ctx       = map_ctx,
        .is_new    = is_new,
    };
}

static void *
ezrc_filter_iterator_next (struct ezrc_iterator iterator)
{
    if (iterator.ctx)
    {
        struct ezrc_filter_iterator_ctx *filter_ctx = iterator.ctx;
        void *elm = ezrc_iterator_next (filter_ctx->intern_iter);
        while ((elm) && (!filter_ctx->filter_f (elm, filter_ctx->extern_args)))
        {
            elm = ezrc_iterator_next (filter_ctx->intern_iter);
        }
        return elm;
    }
    return NULL;
}

static void
ezrc_filter_iterator_free (struct ezrc_allocator allocator, void *ctx)
{
    if (ctx)
    {
        struct ezrc_filter_iterator_ctx *filter_ctx = ctx;
        filter_ctx->intern_iter.is_new = NULL;
        ezrc_allocator_free (allocator, filter_ctx);
    }
}

const struct ezrc_iterable filter_iterable = {
    .next      = ezrc_filter_iterator_next,
    .init_ctx  = NULL,
    .free_ctx  = ezrc_filter_iterator_free,
    .is_at_end = NULL,
};

struct ezrc_iterator
ezrc_iterator_filter (struct ezrc_iterator iterator, int (*filter_f) (void *, void *), void *extern_args)
{
    struct ezrc_filter_iterator_ctx *filter_ctx = ezrc_allocator_alloc (iterator.allocator, sizeof (struct ezrc_filter_iterator_ctx));
    if (filter_ctx)
    {
        filter_ctx->filter_f    = filter_f;
        filter_ctx->intern_iter = iterator;
        filter_ctx->extern_args = extern_args;
    }
    int *is_new = ezrc_allocator_alloc (iterator.allocator, sizeof (int));
    if (is_new) *is_new = 2;
    return (struct ezrc_iterator) {
        .allocator = iterator.allocator,
        .iterable  = &filter_iterable,
        .ctx       = filter_ctx,
        .is_new    = is_new,
    };
}

void
ezrc_iterator_fold (struct ezrc_iterator iterator, void *accumulator, void (*fold_f) (void *acc, void *elm, void *args), void *extern_args)
{
    void *elm;
    while ((elm = ezrc_iterator_next (iterator)))
    {
        fold_f (accumulator, elm, extern_args);
    }
}
