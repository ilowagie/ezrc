#include "ezrc/containers/flex_array.h"

#include "ezrc/memory/allocator.h"
#include "ezrc/macro_util.h"

void *
ezrc_flex_array_iter_next (struct ezrc_iterator iterator)
{
    if ((!iterator.iterable) || (!iterator.ctx)) return NULL;
    struct ezrc_flex_array *array   = ezrc_struct_of (iterator.iterable, struct ezrc_flex_array, iterable);
    size_t                 *cur_idx = iterator.ctx;
    if ((*cur_idx) < array->len)
        return ezrc_flex_array_get_elm__internal (array, (*cur_idx)++);
    else
        return NULL;
}

void *
ezrc_flex_array_iter_init_ctx (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)
{
    size_t *cur_idx = ezrc_allocator_alloc (allocator, sizeof (size_t));
    if (cur_idx)
        *cur_idx = 0;
    return cur_idx;
}

void
ezrc_flex_array_iter_free_ctx (struct ezrc_allocator allocator, void *iterator_ctx)
{
    ezrc_allocator_free (allocator, iterator_ctx);
}

int
ezrc_flex_array_iter_is_at_end (struct ezrc_iterator iterator)
{
    if ((!iterator.iterable) || (!iterator.ctx)) return 1;
    struct ezrc_flex_array *array   = ezrc_struct_of (iterator.iterable, struct ezrc_flex_array, iterable);
    size_t                 *cur_idx = iterator.ctx;
    if (cur_idx) return (*cur_idx) == array->len;
    return 0;
}

struct ezrc_return
ezrc_init_flex_array (struct ezrc_flex_array *array, struct ezrc_allocator allocator, const size_t init_cap, const size_t elm_size)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    array->threadsafe = 0;
    size_t init_size  = init_cap * elm_size;
    array->array      = ezrc_allocator_alloc (allocator, init_size);
    if (!array->array)
    {
        ezrc_allocator_free (allocator, array);
        return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    }
    array->cap       = init_cap;
    array->len       = 0;
    array->elm_size  = elm_size;
    array->allocator = allocator;
    array->iterable  = (struct ezrc_iterable){
        .next      = ezrc_flex_array_iter_next,
        .init_ctx  = ezrc_flex_array_iter_init_ctx,
        .free_ctx  = ezrc_flex_array_iter_free_ctx,
        .is_at_end = ezrc_flex_array_iter_is_at_end,
    };

    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_flex_array_room_for_one_more__unsafe (struct ezrc_flex_array *array)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    struct ezrc_return cur_ret = EZRC_RETURN (EZRC_OK);
    if (array->cap >= (array->len + 1)) goto function_end;
    size_t  new_cap   = array->cap << 1;
    size_t  new_size  = new_cap * array->elm_size;
    void   *new_array = ezrc_allocator_realloc (array->allocator, array->array, new_size);
    if (!new_array)
    {
        new_cap   = array->cap + 1;
        new_size  = new_cap * array->elm_size;
        new_array = ezrc_allocator_realloc (array->allocator, array->array, new_size);
        if (!new_array)
        {
            cur_ret = EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
            goto function_end;
        }
    }
    array->array = new_array;
    array->cap   = new_cap;

function_end:
    return cur_ret;
}

struct ezrc_return
ezrc_flex_array_room_for_one_more (struct ezrc_flex_array *array)
{
#ifdef EZRC_HAS_PTHREAD
    if (array->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)array;
        if (pthread_rwlock_wrlock (&array_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    struct ezrc_return ret = ezrc_flex_array_room_for_one_more__unsafe (array);
#ifdef EZRC_HAS_PTHREAD
    if (array->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)array;
        pthread_rwlock_unlock (&array_ts->rwlock);
    }
#endif
    return ret;
}

void *
ezrc_flex_array_get_elm__internal (const struct ezrc_flex_array *array, const size_t idx)
{
    if (!array->array)     return NULL;
#ifdef EZRC_HAS_PTHREAD
    if (array->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)array;
        if (pthread_rwlock_rdlock (&array_ts->rwlock)) return NULL;
    }
#endif
    void *value = NULL;
    if (idx >= array->len) goto function_end;
    size_t offset = idx * array->elm_size;
    value = ((uint8_t *)array->array + offset);
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (array->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)array;
        pthread_rwlock_unlock (&array_ts->rwlock);
    }
#endif
    return value;
}

struct ezrc_return
ezrc_cleanup_flex_array (struct ezrc_flex_array *array)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (array->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)array;
        if (pthread_rwlock_destroy (&array_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
    ezrc_allocator_free (array->allocator, array->array);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_flex_string_concat (struct ezrc_flex_array *flex_str, const char *str)
{
    if (!flex_str) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    struct ezrc_return cur_ret = EZRC_RETURN (EZRC_OK);
    if (str[0] == '\0') goto function_end; /* nothing to append */
#ifdef EZRC_HAS_PTHREAD
    if (flex_str->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)flex_str;
        if (pthread_rwlock_wrlock (&array_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    if (!flex_str->array)
    {
        cur_ret = EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
        goto function_end;
    }
    if (flex_str->elm_size != sizeof (char))
    {
        cur_ret = EZRC_RETURN (EZRC_ERR_INTERNAL_CONTRADICTION);
        goto function_end;
    }
    /* overwrite previous \0 with first char of the string to append if there was already something present */
    if (flex_str->len > 0)
        ezrc_flex_array_set__unsafe (char, flex_str, str[0], (flex_str->len - 1));
    else
        ezrc_flex_array_append__unsafe (char, flex_str, str[0]);
    size_t idx = 0;
    do
    {
        idx++;
        ezrc_flex_array_append__unsafe (char, flex_str, str[idx]);
    } while (str[idx] != '\0');
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (flex_str->threadsafe)
    {
        struct ezrc_flex_array_ts *array_ts = (struct ezrc_flex_array_ts *)flex_str;
        pthread_rwlock_unlock (&array_ts->rwlock);
    }
#endif
    return cur_ret;
}

#ifdef EZRC_HAS_PTHREAD

struct ezrc_return
ezrc_init_flex_array_ts (struct ezrc_flex_array_ts *array, struct ezrc_allocator allocator, const size_t init_cap, const size_t elm_size)
{
    struct ezrc_return ret = ezrc_init_flex_array ((struct ezrc_flex_array *)array, allocator, init_cap, elm_size);
    if (ret.code != EZRC_OK) return ret;
    array->threadsafe = 1;
    if (pthread_rwlock_init (&array->rwlock, NULL)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_array_get_rd_lock (struct ezrc_flex_array_ts *array)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!array->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_rdlock (&array->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_array_get_wr_lock (struct ezrc_flex_array_ts *array)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!array->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_wrlock (&array->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_array_release_lock (struct ezrc_flex_array_ts *array)
{
    if (!array) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!array->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    pthread_rwlock_unlock (&array->rwlock);
    return EZRC_RETURN (EZRC_OK);
}

#endif
