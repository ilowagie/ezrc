#include "ezrc/containers/linked_list.h"

#include "ezrc/base_structs.h"
#include "ezrc/memory/allocator.h"
#include "ezrc/macro_util.h"

void *
ezrc_linked_list_iter_next (struct ezrc_iterator iterator)
{
    if (!iterator.ctx) return NULL;
    struct ezrc_linked_list_node **cur_node = iterator.ctx;
    struct ezrc_linked_list_node *node_to_return = *cur_node;
    if (node_to_return) *cur_node = node_to_return->next;
    return node_to_return;
}

void *
ezrc_linked_list_iter_init_ctx (struct ezrc_allocator allocator, const struct ezrc_iterable *iterable)
{
    struct ezrc_linked_list_node **cur_node = ezrc_allocator_alloc (allocator, sizeof (struct ezrc_linked_list_node *));
    struct ezrc_linked_list_head *list = ezrc_struct_of (iterable, struct ezrc_linked_list_head, iterable);
    if (cur_node)
        *cur_node = list->first;
    return cur_node;
}

void
ezrc_linked_list_iter_free_ctx (struct ezrc_allocator allocator, void *iterator_ctx)
{
    ezrc_allocator_free (allocator, iterator_ctx);
}

int
ezrc_linked_list_iter_is_at_end (struct ezrc_iterator iterator)
{
    if (!iterator.ctx) return 1;
    struct ezrc_linked_list_node **cur_node = iterator.ctx;
    return ((*cur_node) == NULL);
}

struct ezrc_return
ezrc_init_linked_list (struct ezrc_linked_list_head *list)
{
    if (!list) return EZRC_RETURN(EZRC_ERR_NULL_PTR_ACCESS);

    list->first    = NULL;
    list->iterable = (struct ezrc_iterable) {
        .next      = ezrc_linked_list_iter_next,
        .init_ctx  = ezrc_linked_list_iter_init_ctx,
        .free_ctx  = ezrc_linked_list_iter_free_ctx,
        .is_at_end = ezrc_linked_list_iter_is_at_end,
    };
    list->threadsafe = 0;

    return EZRC_RETURN(EZRC_OK);
}

struct ezrc_return
ezrc_linked_list_append_back (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node)
{
    if ((!head) || (!node)) return EZRC_RETURN(EZRC_ERR_NULL_PTR_ACCESS);
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_wrlock (&head_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif

    node->next = NULL;
    struct ezrc_linked_list_node **cur = &head->first;
    while (*cur != NULL)
        cur = &((*cur)->next);
    *cur = node;
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return EZRC_RETURN(EZRC_OK);
}

struct ezrc_return
ezrc_linked_list_append_front (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node)
{
    if ((!head) || (!node)) return EZRC_RETURN(EZRC_ERR_NULL_PTR_ACCESS);
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_wrlock (&head_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif

    node->next = head->first;
    head->first = node;

#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return EZRC_RETURN(EZRC_OK);
}

struct ezrc_linked_list_node *
ezrc_linked_list_pop_back (struct ezrc_linked_list_head *head)
{
    if (!head) return NULL;
    struct ezrc_linked_list_node *node = NULL;

#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_wrlock (&head_ts->rwlock)) return NULL;
    }
#endif
    if (!head->first) goto function_end;

    struct ezrc_linked_list_node **cur = &head->first;
    while ((*cur)->next != NULL)
        cur = &((*cur)->next);
    node = *cur;
    *cur = NULL;

function_end:
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return node;
}

struct ezrc_linked_list_node *
ezrc_linked_list_pop_front (struct ezrc_linked_list_head *head)
{
    if (!head) return NULL;
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_wrlock (&head_ts->rwlock)) return NULL;
    }
#endif
    struct ezrc_linked_list_node *node = head->first;
    if (node) head->first = node->next;
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return node;
}

struct ezrc_return
ezrc_linked_list_remove (struct ezrc_linked_list_head *head, struct ezrc_linked_list_node *node)
{
    struct ezrc_return cur_ret = EZRC_RETURN (EZRC_OK);
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_wrlock (&head_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    struct ezrc_linked_list_node **cur_node = &head->first;
    while (*cur_node != NULL)
    {
        if (*cur_node == node)
        {
            *cur_node = node->next;
            goto function_end;
        }
        cur_node = &((*cur_node)->next);
    }
    cur_ret = EZRC_RETURN (EZRC_ERR_NOT_FOUND);
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return cur_ret;
}

struct ezrc_linked_list_node *
ezrc_linked_list_last (struct ezrc_linked_list_head *head)
{
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        if (pthread_rwlock_rdlock (&head_ts->rwlock)) return NULL;
    }
#endif
    struct ezrc_linked_list_node *cur = head->first;
    while (cur && cur->next) cur = cur->next;
#ifdef EZRC_HAS_PTHREAD
    if (head->threadsafe)
    {
        struct ezrc_linked_list_head_ts *head_ts = (struct ezrc_linked_list_head_ts *)head;
        pthread_rwlock_unlock (&head_ts->rwlock);
    }
#endif
    return cur;
}

#ifdef EZRC_HAS_PTHREAD

struct ezrc_return
ezrc_init_linked_list_ts (struct ezrc_linked_list_head_ts *list)
{
    struct ezrc_return ret = ezrc_init_linked_list ((struct ezrc_linked_list_head *)list);
    if (ret.code != EZRC_OK) return ret;
    list->threadsafe = 1;
    if (pthread_rwlock_init (&list->rwlock, NULL)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_linked_list_get_rd_lock (struct ezrc_linked_list_head_ts *head)
{
    if (!head) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!head->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_rdlock (&head->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_linked_list_get_wr_lock (struct ezrc_linked_list_head_ts *head)
{
    if (!head) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!head->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_wrlock (&head->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_linked_list_release_lock (struct ezrc_linked_list_head_ts *head)
{
    if (!head) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!head->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    pthread_rwlock_unlock (&head->rwlock);
    return EZRC_RETURN (EZRC_OK);
}

#endif
