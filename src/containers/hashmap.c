#include <string.h>

#include <ezrc/containers/hashmap.h>
#include <ezrc/memory/allocator.h>
#include <ezrc/macro_util.h>

size_t
ezrc_hashmap_str_len (const void *str)
{
    return (strlen ((char *)str) + 1) * sizeof (char);
}

struct ezrc_return
ezrc_hashmap_init (struct ezrc_allocator allocator, struct ezrc_hashmap *map, struct ezrc_hashmap_cfg cfg)
{
    map->allocator = allocator;
    map->cfg       = cfg;
    map->filling   = 0;
    map->nrof_rows = cfg.initial_nrof_rows;
    map->rows      = ezrc_allocator_alloc (allocator, cfg.initial_nrof_rows * sizeof (struct ezrc_hashmap_row));
    map->cur_size_check_itv = cfg.size_check_intv;
    if (!map->rows) return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    for (size_t i = 0; i < cfg.initial_nrof_rows; i++) ezrc_init_linked_list (&map->rows[i].ll_elm_head);
#ifdef EZRC_HAS_PTHREAD
    map->threadsafe = 0;
#endif
    return EZRC_RETURN (EZRC_OK);
}

size_t
ezrc__hashmap_get_key_len (const struct ezrc_hashmap_cfg cfg, const void *key, struct ezrc_return *ret)
{
    if (cfg.sizeof_key != 0) return cfg.sizeof_key;
    else if (cfg.get_key_len) return cfg.get_key_len (key);
    else ezrc_set_opt_error (ret, EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS));
    return 0;
}

size_t
ezrc__hashmap_get_val_len (const struct ezrc_hashmap_cfg cfg, const void *val, struct ezrc_return *ret)
{
    if (cfg.sizeof_val != 0) return cfg.sizeof_val;
    else if (cfg.get_val_len) return cfg.get_val_len (val);
    else ezrc_set_opt_error (ret, EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS));
    return 0;
}

int
ezrc_hashmap_check_elm (const struct ezrc_hashmap_cfg cfg, struct ezrc_hashmap_elm *elm, uint64_t hash, const void *key)
{
    if (elm->hash != hash) return 0;
    if (!cfg.memcmp_after_col) return 1;
    void *elm_key = NULL;
    size_t elm_key_len = 0;
    if (cfg.store_key)
    {
        elm_key = ((void *)elm) + offsetof (struct ezrc_hashmap_elm, key_val);
        elm_key_len = elm->key_len;
    } else {
        elm_key = elm->key_ptr;
        elm_key_len = ezrc__hashmap_get_key_len (cfg, elm_key, NULL);
    }
    size_t key_len = ezrc__hashmap_get_key_len (cfg, key, NULL);
    if (key_len != elm_key_len) return 0;
    return (memcmp (key, elm_key, key_len) == 0);
}

void *
ezrc_hashmap_get_val_from_elm (const struct ezrc_hashmap_cfg cfg, struct ezrc_hashmap_elm *elm)
{
    if (!cfg.store_val) return elm->val_ptr;
    else if (cfg.store_key) return ((void *)elm) + offsetof(struct ezrc_hashmap_elm, key_val) + elm->key_len;
    else return ((void *)elm) + offsetof (struct ezrc_hashmap_elm, key_val);
}

struct ezrc_return
ezrc_hashmap_write_elm (const struct ezrc_hashmap map,
                        void *key,
                        const size_t key_len,
                        void *val,
                        const size_t val_len,
                        struct ezrc_hashmap_elm **elm)
{
    size_t elm_size = sizeof (struct ezrc_hashmap_elm) - sizeof (void *);
    if (map.cfg.store_key) elm_size += key_len;
    if (map.cfg.store_val) elm_size += val_len;
    struct ezrc_hashmap_elm *new_elm;
    if (*elm) new_elm = ezrc_allocator_realloc (map.allocator, *elm, elm_size);
    else new_elm = ezrc_allocator_alloc (map.allocator, elm_size);
    if (!new_elm) return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    *elm = new_elm;
    void *val_ptr = ((void *)*elm) + offsetof (struct ezrc_hashmap_elm, key_val);
    if (map.cfg.store_key)
    {
        void *key_ptr = val_ptr;
        val_ptr += key_len;
        memcpy (key_ptr, key, key_len);
        new_elm->key_len = key_len;
    } else {
        new_elm->key_ptr = key;
    }
    if (map.cfg.store_val)
    {
        memcpy (val_ptr, val, val_len);
    } else {
        new_elm->val_ptr = val;
    }
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_grow (struct ezrc_hashmap *map)
{
    if (map->nrof_rows >= UINT64_MAX)
        return EZRC_RETURN (EZRC_ERR_MAX_SIZE_REACHED);
    size_t old_nrof_rows = map->nrof_rows;
    size_t new_nrof_rows = old_nrof_rows << 1;
    struct ezrc_hashmap_row *new_row = ezrc_allocator_realloc (map->allocator, map->rows, new_nrof_rows * sizeof (struct ezrc_hashmap_row));
    if (!new_row)
        return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    /* number of rows was succesfully increased: update hashmap values */
    map->nrof_rows = new_nrof_rows;
    map->rows = new_row;
    /* check for each element if it needs to be moved */
    size_t i_moved;
    struct ezrc_linked_list_node **cur;
    struct ezrc_hashmap_elm *elm;
    for (size_t i = old_nrof_rows; i < map->nrof_rows; i++)
    {
        ezrc_init_linked_list (&map->rows[i].ll_elm_head);
    }
    for (size_t i = 0; i < old_nrof_rows; i++)
    {
        i_moved = i + old_nrof_rows;
        cur = &map->rows[i].ll_elm_head.first;
        while (*cur)
        {
            elm = ezrc_struct_of (*cur, struct ezrc_hashmap_elm, ll);
            /* if the highest relevant bit of the hash is 1, the modulo is now different for the new amount of rows */
            if ((elm->hash >> (map->nrof_rows - 1)) & 1)
            {
                *cur = elm->ll.next; /* remove element and update cur */
                ezrc_linked_list_append_back (&map->rows[i_moved].ll_elm_head, &elm->ll);
            } else {
                cur = &elm->ll.next; /* only update cur */
            }
        }
    }
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_shrink (struct ezrc_hashmap *map)
{
    if (map->nrof_rows % 2 != 0)
        return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION); /* needs to be divisible by 2 */
    size_t old_nrof_rows = map->nrof_rows;
    size_t new_nrof_rows = old_nrof_rows >> 1;
    /* attempt to allocate a smaller row array and fill it with the values in the old row first */
    struct ezrc_hashmap_row *new_row = ezrc_allocator_alloc (map->allocator, new_nrof_rows * sizeof (struct ezrc_hashmap_row));
    if (!new_row)
        return EZRC_RETURN (EZRC_ERR_ALLOC_FAIL);
    for (size_t i = 0; i < new_nrof_rows; i++)
    {
        /* elements fitting in the smaller nrof rows can remain in the same row */
        ezrc_init_linked_list (&new_row[i].ll_elm_head);
        new_row[i].ll_elm_head.first = map->rows[i].ll_elm_head.first;
    }
    size_t i_moved;
    for (size_t i = new_nrof_rows; i < old_nrof_rows; i++)
    {
        /* elements outside new nrof rows need to move the row difference back */
        i_moved = i - new_nrof_rows;
        if (new_row[i_moved].ll_elm_head.first)
        {
            ezrc_linked_list_last (&new_row[i_moved].ll_elm_head)->next = map->rows[i].ll_elm_head.first;
        } else {
            new_row[i_moved].ll_elm_head.first = map->rows[i].ll_elm_head.first;
        }
    }
    /* done updating the new rows: put them in the map and free the old rows */
    ezrc_allocator_free (map->allocator, map->rows);
    map->nrof_rows = new_nrof_rows;
    map->rows = new_row;
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_put (struct ezrc_hashmap *map, void *key, void *val)
{
    struct ezrc_return cur_ret = EZRC_RETURN (EZRC_OK);
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        if (pthread_rwlock_rdlock (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
        /* first get a read lock: we need to find where to place the new element */
    }
#endif
    size_t key_len = ezrc__hashmap_get_key_len (map->cfg, key, &cur_ret);
    size_t val_len = ezrc__hashmap_get_val_len (map->cfg, val, &cur_ret);
    if (cur_ret.code != EZRC_OK) goto function_end;
    /* find place to put element */
    uint64_t hash = XXH64 (key, key_len, EZRC_HASHMAP_HASH_SEED);
    size_t idx = hash % map->nrof_rows;
    struct ezrc_linked_list_node **cur_node = &map->rows[idx].ll_elm_head.first;
    while (*cur_node)
    {
        struct ezrc_hashmap_elm *elm = ezrc_struct_of ((*cur_node), struct ezrc_hashmap_elm, ll);
        if (ezrc_hashmap_check_elm (map->cfg, elm, hash, key))
        {
            /* if we get in this point the function will exit */
#ifdef EZRC_HAS_PTHREAD
            if (map->threadsafe)
            {
                /* place for the element has been found: change lock to a write lock */
                struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
                pthread_rwlock_unlock (&map_ts->rwlock);
                if (pthread_rwlock_wrlock (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
            }
#endif
            if (map->cfg.allow_overwrite) cur_ret = ezrc_hashmap_write_elm (*map, key, key_len, val, val_len, &elm);
            else cur_ret = EZRC_RETURN (EZRC_ERR_UNIQUE_VIOLATION);
            goto function_end;
        }
        cur_node = &(*cur_node)->next;
    }
    /* NOTE: if we get here, the rwlock is locked in READ mode => need to relock in write mode */
    struct ezrc_hashmap_elm *new_elm = NULL;
    if (map->threadsafe)
    {
        /* change lock to a write lock */
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        pthread_rwlock_unlock (&map_ts->rwlock);
        if (pthread_rwlock_wrlock (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
    cur_ret = ezrc_hashmap_write_elm (*map, key, key_len, val, val_len, &new_elm);
    if (cur_ret.code != EZRC_OK) return cur_ret;
    new_elm->hash = hash;
    new_elm->ll.next = NULL;
    *cur_node = &new_elm->ll;
    map->filling++;
    /* check filling if resizing is allowed */
    if (map->cfg.allow_resize && map->cur_size_check_itv == 0)
    {
        double load_factor = ((double)map->filling) / map->nrof_rows;
        if (load_factor > map->cfg.max_load_factor) cur_ret = ezrc_hashmap_grow (map);
    }
    map->cur_size_check_itv = (map->cur_size_check_itv == 0) ? map->cfg.size_check_intv : map->cur_size_check_itv - 1;
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        pthread_rwlock_unlock (&map_ts->rwlock);
    }
#endif
    return cur_ret;
}

void *
ezrc_hashmap_get (const struct ezrc_hashmap *map, const void *key, struct ezrc_return *opt_err)
{
    void *value = NULL;
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        if (pthread_rwlock_rdlock (&map_ts->rwlock))
        {
            ezrc_set_opt_error (opt_err, EZRC_RETURN (EZRC_ERR_BAD_MTX));
            return NULL;
        }
    }
#endif
    size_t key_len = ezrc__hashmap_get_key_len (map->cfg, key, opt_err);
    if (key_len == 0) goto function_end;
    uint64_t hash = XXH64 (key, key_len, EZRC_HASHMAP_HASH_SEED);
    size_t idx = hash % map->nrof_rows;
    struct ezrc_linked_list_node *cur_node = map->rows[idx].ll_elm_head.first;
    while (cur_node)
    {
        struct ezrc_hashmap_elm *elm = ezrc_struct_of (cur_node, struct ezrc_hashmap_elm, ll);
        if (ezrc_hashmap_check_elm (map->cfg, elm, hash, key))
        {
            value = ezrc_hashmap_get_val_from_elm (map->cfg, elm);
            goto function_end;
        }
        cur_node = cur_node->next;
    }
    ezrc_set_opt_error (opt_err, EZRC_RETURN (EZRC_ERR_NOT_FOUND));

function_end:
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        pthread_rwlock_unlock (&map_ts->rwlock);
    }
#endif
    return value;
}

struct ezrc_return
ezrc_hashmap_del (struct ezrc_hashmap *map, void *key)
{
    struct ezrc_return cur_ret = EZRC_RETURN (EZRC_OK);
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        /* first get a read lock */
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        if (pthread_rwlock_rdlock (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    size_t key_len = ezrc__hashmap_get_key_len (map->cfg, key, &cur_ret);
    if (cur_ret.code != EZRC_OK) goto function_end;
    uint64_t hash = XXH64 (key, key_len, EZRC_HASHMAP_HASH_SEED);
    size_t idx = hash % map->nrof_rows;
    struct ezrc_linked_list_node **cur_node = &map->rows[idx].ll_elm_head.first;
    while (*cur_node)
    {
        struct ezrc_hashmap_elm *elm = ezrc_struct_of ((*cur_node), struct ezrc_hashmap_elm, ll);
        if (ezrc_hashmap_check_elm (map->cfg, elm, hash, key))
        {
#ifdef EZRC_HAS_PTHREAD
            /* element found: change read lock to a write lock */
            if (map->threadsafe)
            {
                struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
                pthread_rwlock_unlock (&map_ts->rwlock);
                if (pthread_rwlock_wrlock (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
            }
#endif
            *cur_node = (*cur_node)->next;
            ezrc_allocator_free (map->allocator, elm);
            map->filling--;
            /* check filling if resizing is allowed */
            if (map->cfg.allow_resize && map->cur_size_check_itv == 0)
            {
                double load_factor = ((double)map->filling) / map->nrof_rows;
                if (load_factor > map->cfg.min_load_factor) cur_ret = ezrc_hashmap_shrink (map);
            }
            map->cur_size_check_itv = (map->cur_size_check_itv == 0) ? map->cfg.size_check_intv : map->cur_size_check_itv - 1;
            goto function_end;
        }
        cur_node = &(*cur_node)->next;
    }
    cur_ret = EZRC_RETURN (EZRC_ERR_NOT_FOUND);
function_end:
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        pthread_rwlock_unlock (&map_ts->rwlock);
    }
#endif
    return cur_ret;
}

struct ezrc_return
ezrc_hashmap_deinit (struct ezrc_hashmap *map)
{
#ifdef EZRC_HAS_PTHREAD
    if (map->threadsafe)
    {
        struct ezrc_hashmap_ts *map_ts = (struct ezrc_hashmap_ts *)map;
        if (pthread_rwlock_destroy (&map_ts->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    }
#endif
    for (size_t i = 0; i < map->nrof_rows; i++)
    {
        struct ezrc_linked_list_node *cur = map->rows[i].ll_elm_head.first;
        while (cur)
        {
            struct ezrc_hashmap_elm *elm = ezrc_struct_of (cur, struct ezrc_hashmap_elm, ll);
            cur = cur->next;
            ezrc_allocator_free (map->allocator, elm);
        }
    }
    ezrc_allocator_free (map->allocator, map->rows);
    return EZRC_RETURN (EZRC_OK);
}

#ifdef EZRC_HAS_PTHREAD

struct ezrc_return
ezrc_hashmap_init_ts (struct ezrc_allocator allocator, struct ezrc_hashmap_ts *map, struct ezrc_hashmap_cfg cfg)
{
    struct ezrc_return ret = ezrc_hashmap_init (allocator, (struct ezrc_hashmap *)map, cfg);
    if (ret.code != EZRC_OK) return ret;
    map->threadsafe = 1;
    if (pthread_rwlock_init (&map->rwlock, NULL)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_get_rd_lock (struct ezrc_hashmap_ts *hashmap)
{
    if (!hashmap) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!hashmap->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_rdlock (&hashmap->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_get_wr_lock (struct ezrc_hashmap_ts *hashmap)
{
    if (!hashmap) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!hashmap->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    if (pthread_rwlock_wrlock (&hashmap->rwlock)) return EZRC_RETURN (EZRC_ERR_BAD_MTX);
    return EZRC_RETURN (EZRC_OK);
}

struct ezrc_return
ezrc_hashmap_release_lock (struct ezrc_hashmap_ts *hashmap)
{
    if (!hashmap) return EZRC_RETURN (EZRC_ERR_NULL_PTR_ACCESS);
    if (!hashmap->threadsafe) return EZRC_RETURN (EZRC_ERR_UNSUPPORTED_ACTION);
    pthread_rwlock_unlock (&hashmap->rwlock);
    return EZRC_RETURN (EZRC_OK);
}

#endif
