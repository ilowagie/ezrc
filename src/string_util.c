#include "ezrc/string_util.h"
#include "ezrc/memory/allocator.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

struct ezrc_return
ezrc_create_format_string (struct ezrc_allocator allocator, char **buf, char *fmt, ...)
{
    struct ezrc_return info;
    va_list ap1, ap2;
    va_start (ap1, fmt);
    va_copy  (ap2, ap1);

    size_t buf_size = 0;
    int    n        = vsnprintf (*buf, buf_size, fmt, ap1);
    if (n < 0)
    {
        /* GCOVR_EXCL_START */
        info = EZRC_RETURN(EZRC_ERR_STD_STR);
        goto fail;
        /* GCOVR_EXCL_STOP */
    }

    /* update buf_size and allocate needed size */
    buf_size = (size_t) n + 1;
    *buf     = ezrc_allocator_alloc (allocator, buf_size);
    if (!(*buf))
    {
        info = EZRC_RETURN(EZRC_ERR_ALLOC_FAIL);
    }
    else
    {
        n = vsnprintf (*buf, buf_size, fmt, ap2);
        if (n < 0)
        {
            /* GCOVR_EXCL_START */
            ezrc_allocator_free (allocator, *buf);
            *buf = NULL;
            info = EZRC_RETURN(EZRC_ERR_STD_STR);
            goto fail;
            /* GCOVR_EXCL_STOP */
        }
        va_end (ap1);
        va_end (ap2);
        return EZRC_RETURN(EZRC_OK);
    }
fail:
    va_end (ap1);
    va_end (ap2);
    return info;
}

char *
ezrc_strdup_static (struct ezrc_allocator allocator, const char *string, struct ezrc_return *info)
{
    size_t alloc_len = strlen (string) + 1;
    char *cpy_string = ezrc_allocator_alloc (allocator, alloc_len);
    if (cpy_string) strncpy (cpy_string, string, alloc_len);
    else ezrc_set_opt_error (info, EZRC_RETURN (EZRC_ERR_ALLOC_FAIL));
    return cpy_string;
}
