#include "ezrc/error_codes.h"

const char *ezrc_error_code_strings_c[] = {
    "success",
    "failed allocation, NULL pointer returned",
    "catched trying to access a NULL pointer",
    "error in a libc string function",
    "error during file operations, relevant errno might be set",
    "a fatal internal contradiction has been found, something's possibly corrrupt",
    "whatever you're trying to do, somehow, it's not supported",
    "the value you're looking for has not been found",
    "a mutex function has failed with EINVAL: the mutex has not been properly initialized",
    "something that has to be unique in a set seems to not be unique. A double has been found",
    "you've reached the maximum size of something",
};
